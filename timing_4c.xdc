############################### Definition of clocks used in the desing #######################################

# The 100 Mhz PCIe clock
create_clock -period 16.000 -name pcie_clk1_p -waveform {0.000 8.000} [get_ports pcie_clk1_p]

# The clock for the SERDES
create_clock -period 6.402 -name qpll_ref_clock_p -waveform {0.000 3.201} [get_ports qpll_ref_clock_p]

# The clock which goes into the clock generator which in turn generates the universal 100MHz clock and the serdes clock for the sender.
create_clock -period 4.000 -name u250mhz_clk1_p -waveform {0.000 2.000} [get_ports {u250mhz_clk1_p}]

## The clock on the receiving end from the SERDES: the recovered clock driving the SLINK receiver logic
#create_clock -period 5.120 -name rxoutclk_out[0]_2 -waveform {0.000 2.560} [get_pins {slink_interface/slink_receiver_c0/U0/serdes_receiver_i1/serdes_i1/inst/gen_gtwizard_gtye4_top.SlinkRocket_SERDES_Main_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.bufg_gt_usrclk2_inst/I}]
#create_clock -period 5.120 -name rxoutclk_out[0]_2 -waveform {0.000 2.560} [get_pins {slink_interface/slink_receiver_c1/U0/serdes_receiver_i1/serdes_i1/inst/gen_gtwizard_gtye4_top.SlinkRocket_SERDES_Main_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.bufg_gt_usrclk2_inst/I}]
#create_clock -period 5.120 -name rxoutclk_out[0]_2 -waveform {0.000 2.560} [get_pins {slink_interface/slink_receiver_c2/U0/serdes_receiver_i1/serdes_i1/inst/gen_gtwizard_gtye4_top.SlinkRocket_SERDES_Main_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.bufg_gt_usrclk2_inst/I}]
#create_clock -period 5.120 -name rxoutclk_out[0]_2 -waveform {0.000 2.560} [get_pins {slink_interface/slink_receiver_c3/U0/serdes_receiver_i1/serdes_i1/inst/gen_gtwizard_gtye4_top.SlinkRocket_SERDES_Main_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.bufg_gt_usrclk2_inst/I}]

############################################ asynchronous clock domains ##############################################################

# The various clocks in this design can be put asynchronous since they are either protected or the relation is logically irrelevant.

# 100MHz clock is independent of all other clocks:

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_rcv_serdes_FED_1]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_rcv_serdes_FED_2]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_rcv_serdes_FED_3]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_rcv_serdes_FED]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_DAQ_1]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_DAQ_2]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_DAQ_3]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_DAQ]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c0/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c1/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c2/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c3/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_FED_1]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_FED_2]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_FED_3]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks clock_trans_serdes_FED]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c0/clk_out]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c1/clk_out]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c2/clk_out]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c3/clk_out]]

set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c0/clk_out]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c0/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c1/clk_out]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c1/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c2/clk_out]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c2/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]
set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c3/clk_out]] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c3/U0/serdes_receiver_i1/serdes_i1/gtwiz_userclk_tx_usrclk2_out[0]]]



# The PCIe clock is independent of these clocks since setting and reading of registers is uncritical wrt meta-stability (we do not care if we very rarely read a bogus value)

set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks -of_objects [get_pins slink_interface/clockdriver/inst/clk_out1]]

set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_rcv_serdes_FED_1]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_rcv_serdes_FED_2]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_rcv_serdes_FED_3]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_rcv_serdes_FED]

set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c0/clk_out]] 
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c1/clk_out]] 
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c2/clk_out]] 
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks -of_objects [get_pins slink_interface/slink_receiver_c3/clk_out]] 

set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_trans_serdes_DAQ_1]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_trans_serdes_DAQ_2]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_trans_serdes_DAQ_3]
set_clock_groups -asynchronous -group [get_clocks pcie_clk] -group [get_clocks clock_trans_serdes_DAQ]

############################# that's all ##############################

