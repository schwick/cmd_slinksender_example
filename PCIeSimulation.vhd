library IEEE;
use IEEE.std_logic_1164.all;

package PCIeSimulation is
  
  procedure PCIe_func(

    signal usr_clock        : in std_logic;
    funct_in	        	: in integer;
    offset                  : in integer;
    rp_wn			        : in std_logic;
    data_b			        : in std_logic_vector(63 downto 0);			
    signal usr_func_wr		: out std_logic_vector(16383 downto 0);
    signal usr_cs_wr		: out std_logic; 
    signal usr_wen		    : out std_logic;
    signal user_data_wr		: out std_logic_vector(63 downto 0);
    signal usr_func_rd		: out std_logic_vector(16383 downto 0);
    signal usr_cs_rd		: out std_logic;
    signal usr_rden		    : out std_logic
    );
  
end PCIeSimulation;

package body PCIeSimulation is

  procedure PCIe_func(
    signal usr_clock   	: in std_logic;
    funct_in	        : in integer;
    offset              : in integer;
    rp_wn	            : in std_logic;
    data_b	            : in std_logic_vector(63 downto 0);
				
    signal usr_func_wr	: out std_logic_vector(16383 downto 0);
    signal usr_cs_wr	: out std_logic; 
    signal usr_wen	    : out std_logic;
    signal user_data_wr	: out std_logic_vector(63 downto 0);
    signal usr_func_rd	: out std_logic_vector(16383 downto 0);
    signal usr_cs_rd	: out std_logic;
    signal usr_rden	    : out std_logic)
  is

    variable funct : integer := funct_in + offset;
    
  begin
	usr_func_wr	   <= (others => '0');
	usr_cs_wr	   <= '0'; 
	usr_wen	           <= '0';
	user_data_wr       <= (others => '0');
	usr_func_rd	   <= (others => '0');
	usr_cs_rd          <= '0';
	usr_rden   	   <= '0';
        
	wait until falling_edge(usr_clock);

    if rp_wn = '1' then
      usr_func_rd(funct)         <= '1';
      usr_cs_rd			 <= '1';
      usr_rden		   	 <= '1';
      wait until falling_edge(usr_clock);
      usr_func_rd(funct)         <= '0';
      usr_cs_rd			 <= '0';
      usr_rden		   	 <= '0';		
	else
      usr_func_wr(funct)         <= '1';
      usr_cs_wr			 <= '1'; 
      usr_wen		   	 <= '1';
      user_data_wr		 <= data_b;
      wait until falling_edge(usr_clock);
      usr_func_wr(funct)         <= '0';
      usr_cs_wr			 <= '0'; 
      usr_wen		   	 <= '0';
      user_data_wr		 <= (others => '0');
	end if;

	wait until falling_edge(usr_clock);
 
  end PCIe_func;
 
end PCIeSimulation;
