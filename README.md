# CMD_SlinkSender_Example

An example design for the CMS community which shows the usage of the SLINK sender core. It also contains a core for an SLINK receiver. (This can be used by others but it contains specific elements used in the CMD firmware eco-system)