----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2019 12:26:00 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
use WORK.top_address_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


entity top is
    Port ( 

      pcie_rst          : in std_logic;
      pcie_clk1_n       : in std_logic;
      pcie_clk1_p       : in std_logic;

      pcie_rx0_p        : in std_logic;
      pcie_rx0_n        : in std_logic;
      pcie_tx0_p        : out std_logic;
      pcie_tx0_n        : out std_logic;
      
      u250mhz_clk1_n    : in std_logic;
      u250mhz_clk1_p    : in std_logic;

      qpll_ref_clock_n  : in std_logic;
      qpll_ref_clock_p  : in std_logic;

      qsfp1_tx1_n       : out std_logic;
      qsfp1_tx1_p       : out std_logic;
      qsfp1_rx1_n       : in std_logic;
      qsfp1_rx1_p       : in std_logic;
        
      qsfp2_tx1_n       : out std_logic;
      qsfp2_tx1_p       : out std_logic;
      qsfp2_rx1_n       : in std_logic;
      qsfp2_rx1_p       : in std_logic;
        
      GPIO_SW_N         : in std_logic;

      GPIO_LED0         : out std_logic;
      GPIO_LED1         : out std_logic;
      --GPIO_LED2         : out std_logic;
      --GPIO_LED3         : out std_logic;
      --GPIO_LED4         : out std_logic;
      --GPIO_LED5         : out std_logic;
      GPIO_LED6         : out std_logic;
      GPIO_LED7         : out std_logic

      );
end top;


--------------------------------------------------------------------------------

architecture Behavioral of top is

-------------------------------------------- Components ---------------------


  component Level_2
    port (
      GPIO_SW_N         : in std_logic;
      u250mhz_clk1_n    : in std_logic;
      u250mhz_clk1_p    : in std_logic;
      pcie_clk          : in std_logic;
      qpll_ref_clock_n  : in std_logic;
      qpll_ref_clock_p  : in std_logic;
      qsfp1_tx1_n       : out std_logic;
      qsfp1_tx1_p       : out std_logic;
      qsfp1_rx1_n       : in std_logic;
      qsfp1_rx1_p       : in std_logic;
      
      qsfp2_tx1_n       : out std_logic;
      qsfp2_tx1_p       : out std_logic;
      qsfp2_rx1_n       : in std_logic;
      qsfp2_rx1_p       : in std_logic;
      
      usr_func_wr       : in std_logic_vector(16383 downto 0);
      usr_wen           : in std_logic;
      usr_data_wr       : in std_logic_vector(63 downto 0);
      usr_func_rd       : in std_logic_vector(16383 downto 0);
      usr_rden          : in std_logic;
      read_data         : out std_logic_vector(63 downto 0);
      
      lff_in            : in std_logic;

      out_clk           : out std_logic;
      link_down         : out std_logic;
      link_full         : out std_logic;

      Rst_hrd_sim       : in std_logic
      );
  end component;
  

  component PCIe_interface
    port(
      pcie_rst	  : in std_logic;
      pcie_clk_n  : in std_logic;
      pcie_clk_p  : in std_logic;
                  
      pcie_tx_n	  : out std_logic;
      pcie_tx_p	  : out std_logic;
      pcie_rx_n	  : in std_logic;
      pcie_rx_p	  : in std_logic;
                  
      usr_clk	  : out std_logic;
      usr_rst_n	  : out std_logic;
                  
      usr_func_wr : out std_logic_vector(16383 downto 0);
      usr_wen	  : out std_logic;
      usr_data_wr : out std_logic_vector(63 downto 0);
                  
      usr_func_rd : out std_logic_vector(16383 downto 0);
      usr_rden	  : out std_logic;
      usr_data_rd : in std_logic_vector(63 downto 0);
      usr_rd_val  : in std_logic
      );
  end component;

-------------------------------- Signals -----------------------------------------

  signal usr_func_wr      : std_logic_vector( 16383 downto 0 );
  signal usr_wen          : std_logic;
  signal usr_data_wr      : std_logic_vector( 63 downto 0 );
  signal usr_func_rd      : std_logic_vector( 16383 downto 0 );
  signal usr_rden         : std_logic;
  signal usr_data_rd      : std_logic_vector( 63 downto 0 );
  signal level2_data      : std_logic_vector( 63 downto 0 );
  signal lff_in           : std_logic;

  signal rddel            : std_logic_vector( 3 downto 0 );
  signal usr_rd_val       : std_logic := '1' ;   -- This should indicate when
                                                 -- data is valid during a read.

  signal pcie_clk         : std_logic;  -- pcie user clock
  signal usr_rst_n        : std_logic;  -- pcie reset

  signal led_1_reg        : std_logic := '0';
  signal led_2_reg        : std_logic := '0';
  
begin

  my_pcie_interface : PCIe_interface
    port map (
      pcie_rst	  => pcie_rst,
      pcie_clk_n  => pcie_clk1_n,
      pcie_clk_p  => pcie_clk1_p,
                  
      pcie_tx_n	  => pcie_tx0_n,
      pcie_tx_p	  => pcie_tx0_p,
      pcie_rx_n	  => pcie_rx0_n,
      pcie_rx_p	  => pcie_rx0_p,
                  
      usr_clk	  => pcie_clk,
      usr_rst_n	  => usr_rst_n,
                  
      usr_func_wr => usr_func_wr,
      usr_wen	  => usr_wen,
      usr_data_wr => usr_data_wr,
                  
      usr_func_rd => usr_func_rd,
      usr_rden	  => usr_rden,
      usr_data_rd => usr_data_rd,

      usr_rd_val  => usr_rd_val
      );

  
  slink_interface : Level_2
    port map (
      GPIO_SW_N         => GPIO_SW_N,
      u250mhz_clk1_n    => u250mhz_clk1_n,   
      u250mhz_clk1_p    => u250mhz_clk1_p,
      pcie_clk          => pcie_clk,
      qpll_ref_clock_n  => qpll_ref_clock_n, 
      qpll_ref_clock_p  => qpll_ref_clock_p,
      qsfp1_tx1_n       => qsfp1_tx1_n,
      qsfp1_tx1_p       => qsfp1_tx1_p,     
      qsfp1_rx1_n       => qsfp1_rx1_n,     
      qsfp1_rx1_p       => qsfp1_rx1_p,     
      
      qsfp2_tx1_n       => qsfp2_tx1_n,
      qsfp2_tx1_p       => qsfp2_tx1_p,     
      qsfp2_rx1_n       => qsfp2_rx1_n,     
      qsfp2_rx1_p       => qsfp2_rx1_p,     

      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,      
      usr_data_wr       => usr_data_wr,     
      usr_func_rd       => usr_func_rd,     
      usr_rden          => usr_rden,      
      read_data         => level2_data,
      
      lff_in            => lff_in,
      link_down         => GPIO_LED7,
      link_full         => GPIO_LED6,

      Rst_hrd_sim       => '0'
      );



-- some test logic for the PCIe interface. Make a LED register
 
  lff_in <= '0';


  -- logic to delay the acknowlege signal to the pci express interface
  rdval : process( pcie_clk, usr_rden, rddel )
    
  begin
    if rising_edge(pcie_clk) then
      if rddel = "0000" then
        if usr_rden = '1'  then
          rddel <= "0001";
        end if;
      else
        rddel(3 downto 1) <= rddel(2 downto 0);
        rddel(0) <= '0';
      end if;
    end if;
  end process;

  usr_rd_val <= rddel(3);

  leds: process( pcie_clk, usr_func_wr, usr_wen )
  begin
    if rising_edge( pcie_clk ) then
      if ( usr_wen = '1' and usr_func_wr( adr_LED_test ) = '1' ) then
        led_1_reg <= usr_data_wr(0);
        led_2_reg <= usr_data_wr(1);
      end if;
    end if;
  end process;

  GPIO_LED0 <= led_1_reg;
  GPIO_LED1 <= led_2_reg;

  -- reading of the leds
  ledread: process( pcie_clk, usr_func_rd )
  begin
    if rising_edge( pcie_clk ) then
      if usr_func_rd( adr_LED_test ) = '1' then
        usr_data_rd(0) <= led_1_reg;
        usr_data_rd(1) <= led_2_reg;
        usr_data_rd(63 downto 2) <= (others => '0');
      else
        usr_data_rd <= level2_data;
      end if;
    end if;
  end process;
                          
  
end Behavioral;
