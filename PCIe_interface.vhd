----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.10.2017 11:43:45
-- Design Name: 
-- Module Name: PCIe_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use UNISIM.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PCIe_interface is
  Port ( 
    pcie_rst	: in std_logic;
    pcie_clk_n	: in std_logic;
    pcie_clk_p	: in std_logic;
    
    pcie_tx_n	: out std_logic;
    pcie_tx_p	: out std_logic;
    pcie_rx_n	: in std_logic;
    pcie_rx_p	: in std_logic;
    
    usr_clk	: out std_logic;
    usr_rst_n	: out std_logic;

    usr_func_wr	: out std_logic_vector(16383 downto 0);
    usr_wen	: out std_logic;
    usr_data_wr	: out std_logic_vector(63 downto 0);
     
    usr_func_rd	: out std_logic_vector(16383 downto 0);
    usr_rden	: out std_logic;
    usr_data_rd	: in std_logic_vector(63 downto 0);
    usr_rd_val	: in std_logic
	
  );
end PCIe_interface;

architecture Behavioral of PCIe_interface is

COMPONENT axi_pcie3_0
  PORT (
    sys_clk          : IN STD_LOGIC;
    sys_clk_gt       : IN STD_LOGIC;
    sys_rst_n        : IN STD_LOGIC;
    user_lnk_up      : OUT STD_LOGIC;
    pci_exp_txp      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_txn      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_rxp      : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_rxn      : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    axi_aclk         : OUT STD_LOGIC;
    axi_aresetn      : OUT STD_LOGIC;
    axi_ctl_aresetn  : OUT STD_LOGIC;
    usr_irq_req      : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    usr_irq_ack      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    msi_enable       : OUT STD_LOGIC;
    msi_vector_width : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_awid      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_awaddr    : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axib_awlen     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axib_awsize    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_awburst   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_awprot    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_awvalid   : OUT STD_LOGIC;
    m_axib_awready   : IN STD_LOGIC;
    m_axib_awlock    : OUT STD_LOGIC;
    m_axib_awcache   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_wdata     : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axib_wstrb     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axib_wlast     : OUT STD_LOGIC;
    m_axib_wvalid    : OUT STD_LOGIC;
    m_axib_wready    : IN STD_LOGIC;
    m_axib_bid       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_bresp     : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_bvalid    : IN STD_LOGIC;
    m_axib_bready    : OUT STD_LOGIC;
    m_axib_arid      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_araddr    : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axib_arlen     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axib_arsize    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_arburst   : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_arprot    : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_arvalid   : OUT STD_LOGIC;
    m_axib_arready   : IN STD_LOGIC;
    m_axib_arlock    : OUT STD_LOGIC;
    m_axib_arcache   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_rid       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_rdata     : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axib_rresp     : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_rlast     : IN STD_LOGIC;
    m_axib_rvalid    : IN STD_LOGIC;
    m_axib_rready    : OUT STD_LOGIC;
    s_axil_awaddr    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axil_awprot    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axil_awvalid   : IN STD_LOGIC;
    s_axil_awready   : OUT STD_LOGIC;
    s_axil_wdata     : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axil_wstrb     : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axil_wvalid    : IN STD_LOGIC;
    s_axil_wready    : OUT STD_LOGIC;
    s_axil_bvalid    : OUT STD_LOGIC;
    s_axil_bresp     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axil_bready    : IN STD_LOGIC;
    s_axil_araddr    : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axil_arprot    : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axil_arvalid   : IN STD_LOGIC;
    s_axil_arready   : OUT STD_LOGIC;
    s_axil_rdata     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axil_rresp     : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axil_rvalid    : OUT STD_LOGIC;
    s_axil_rready    : IN STD_LOGIC;
    interrupt_out    : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT pcie_decoder is
  port
    (
      -- Input ports
      ADDR	    : in  std_logic_vector(16 downto 3);
      BA	    : in  std_logic;
      LD_ADDR	: in  std_logic;
      CLOCK	    : in  std_logic;
      RESET	    : in  std_logic;
      -- Output ports
      SEL_func	: out std_logic_vector(16383 downto 0)
      );
end COMPONENT;
 
COMPONENT resetn_gen is
  generic ( resync : boolean := false);
  port (
    HRD_resetn	: in std_logic;
    clock	: in std_logic;
    func_reset	: in std_logic;
    
    clock_rst	: in std_logic;
    resetn	: out std_logic;
    done	: out std_logic
    );
end COMPONENT;
 
attribute mark_debug : string;

signal pcie_clk_gt	: STD_LOGIC;
signal pcie_clk_ref	: std_logic;

signal Addr_wr		: std_logic_vector(63 downto 0);
signal Addr_wr_latch	: std_logic;
signal Data_wr		: std_logic_vector(63 downto 0);
signal Byte_ena		: std_logic_vector(7 downto 0);
signal Data_wr_latch	: std_logic;
signal Addr_rd		: std_logic_vector(63 downto 0);
signal Addr_rd_latch	: std_logic;

signal rID  		: std_logic_vector(3 downto 0);
signal rID_rg		: std_logic_vector(3 downto 0);
signal wID  		: std_logic_vector(3 downto 0);
signal wID_rg		: std_logic_vector(3 downto 0);


signal data_read	: std_logic_vector(63 downto 0);
signal data_rd_valid	: std_logic;

signal usr_clk_cell	: std_logic;
signal usr_rst_n_cell	: std_logic;

signal usr_wen_reg	: std_logic_vector(3 downto 0);
signal data_wr_reg	: std_logic_vector(63 downto 0);
signal byte_ena_reg	: std_logic_vector(7 downto 0);
signal data_rd_valid_rg	: std_logic;
signal data_read_rg	: std_logic_vector(63 downto 0);
signal usr_rden_reg	: std_logic_vector(3 downto 0);
signal usr_func_wr_cell	: std_logic_vector(16383 downto 0);
signal usr_func_rd_cell	: std_logic_vector(16383 downto 0);
signal soft_reset	: std_logic;
signal local_reset	: std_logic;

signal GND		: std_logic := '0';

--attribute mark_debug of usr_wen_reg: signal is "true"; 

--********************************************************************
--                 CODE START HERE
--********************************************************************
begin

IBUFDS_GTE4_inst : IBUFDS_GTE4
  port map(
    O	   =>  pcie_clk_gt, --1-bitoutput:RefertoTransceiverUserGuide
    ODIV2  =>  pcie_clk_ref,  --1-bitoutput:RefertoTransceiverUserGuide
    CEB	   =>  '0',           --1-bitinput:RefertoTransceiverUserGuide
    I	   =>  pcie_clk_p, 
    IB	   =>  pcie_clk_n 
);
 

PCIe_serdes_itf:axi_pcie3_0
  PORT MAP(
    sys_rst_n         			=>	pcie_rst	     ,--: IN STD_LOGIC;
    sys_clk_gt        			=>	pcie_clk_gt	     ,--: IN STD_LOGIC;
    sys_clk				=>	pcie_clk_ref	     ,
    usr_irq_req(0)     			=>	GND		     ,--: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  usr_irq_ack      			=>			     ,--: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--  user_lnk_up      						     ,--: OUT STD_LOGIC;
--  interrupt_out     			=>			     ,--: OUT STD_LOGIC;
--  msi_enable        			=>			     ,--: OUT STD_LOGIC;
--  msi_vector_width  			=>			     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    pci_exp_rxp(0)     			=>	pcie_rx_p	     ,--: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_rxn(0)     			=>	pcie_rx_n	     ,--: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_txp(0)     			=>	pcie_tx_p	     ,--: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    pci_exp_txn(0)     			=>	pcie_tx_n	     ,--: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    axi_aclk          			=>	usr_clk_cell	     ,--: OUT STD_LOGIC;
    axi_aresetn       			=>	usr_rst_n_cell	     ,--: OUT STD_LOGIC;
--  axi_ctl_aresetn   			=>			     ,--: OUT STD_LOGIC;
    m_axib_awid        			=>	wID		     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_awaddr      			=>	Addr_wr		     ,--: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
--  m_axib_awlen       			=>			     ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--  m_axib_awsize      			=>			     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--  m_axib_awburst     			=>			     ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--  m_axib_awprot      			=>			     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_awvalid     			=>	Addr_wr_latch	     ,--: OUT STD_LOGIC;
--  m_axib_awlock      			=>			     ,--: OUT STD_LOGIC;
--  m_axib_awcache     			=>			     ,--: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_wdata       			=>	Data_wr		     ,--: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
--  m_axib_wuser       			=>			     ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axib_wstrb       			=>	Byte_ena	     ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--  m_axib_wlast       			=>			     ,--: OUT STD_LOGIC;
    m_axib_wvalid      			=>	Data_wr_latch	     ,--: OUT STD_LOGIC;
  
--  m_axib_bready      			=>			     ,--: OUT STD_LOGIC;
    m_axib_arid        			=>	rID		     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_araddr      			=>	Addr_rd		     ,--: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
--  m_axib_arlen       			=>			     ,--: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
--  m_axib_arsize      			=>			     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
--  m_axib_arburst     			=>			     ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--  m_axib_arprot      			=>			     ,--: OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_arvalid     			=>	Addr_rd_latch	     ,--: OUT STD_LOGIC;
--  m_axib_arlock      			=>			     ,--: OUT STD_LOGIC;
--  m_axib_arcache     			=>			     ,--: OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axib_awready     			=>	'1'		     ,--: IN STD_LOGIC;
    m_axib_wready      			=>	'1'		     ,--: IN STD_LOGIC;
    m_axib_bid         			=>	wID_rg		     ,--: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_bresp       			=>	"00"		     ,--: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_bvalid      			=>	usr_wen_reg(3)	     ,--: IN STD_LOGIC;
  
    m_axib_arready     			=>	'1'		     ,--: IN STD_LOGIC;
    m_axib_rid         			=>	rID_rg		     ,--: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axib_rdata       			=>	data_read_rg	     ,--: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axib_rresp       			=>	"00"		     ,--: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axib_rlast       			=>	'1'		     ,--: IN STD_LOGIC;
    m_axib_rvalid      			=>	data_rd_valid_rg     ,--: IN STD_LOGIC;
--interface to intermnal configuration
    s_axil_awaddr  			=>	x"00000000"	     ,--: IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    s_axil_awvalid 			=>	'0'		     ,--: IN STD_LOGIC;
    s_axil_wdata   			=>	x"00000000"	     ,--: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axil_wstrb   			=>	x"0"		     ,--: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axil_wvalid  			=>	'0'		     ,--: IN STD_LOGIC;
    s_axil_bready  			=>	'0'		     ,--: IN STD_LOGIC;
    s_axil_araddr  			=>	x"00000000"	     ,--: IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    s_axil_arvalid 			=>	'0'		     ,--: IN STD_LOGIC;
    s_axil_rready  			=>	'0'		     ,--: IN STD_LOGIC;	 
--  s_axil_awready 			=>			     ,--: OUT STD_LOGIC;
--  s_axil_wready  			=>			     ,--: OUT STD_LOGIC;
--  s_axil_bresp   			=>			     ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--  s_axil_bvalid  			=>			     ,--: OUT STD_LOGIC;
--  s_axil_arready 			=>			     ,--: OUT STD_LOGIC;
--  s_axil_rdata   			=>			     ,--: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--  s_axil_rresp   			=>			     ,--: OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--  s_axil_rvalid  			=>			     ,--: OUT STD_LOGIC,
    s_axil_awprot			=>	"000"		     ,--: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axil_arprot			=>	"000"		      --: IN STD_LOGIC_VECTOR(2 DOWNTO 0);
 );
  
usr_clk		<= usr_clk_cell;
usr_rst_n	<= local_reset;


-- reset Procedure

process(usr_clk_cell)
begin
  if rising_edge(usr_clk_cell) then
    soft_reset	        <= '0';
    if usr_wen_reg(3)    = '1' and usr_func_wr_cell(0) = '1' then
      soft_reset	<=  Data_wr_reg(0);
    end if;
  end if;
end process;
 

gen_rst:resetn_gen  
  port map(
    HRD_resetn	=> usr_rst_n_cell,
    clock	=> usr_clk_cell,
    func_reset	=> soft_reset,
    clock_rst	=> '0',
    resetn	=> local_reset 
    );
  
process(usr_clk_cell)
begin
  if rising_edge(usr_clk_cell) then
    if Addr_wr_latch = '1' then
      wID_rg			<= wID;
    end if;
    
    usr_wen_reg(3 downto 1)	<= usr_wen_reg(2 downto 0);
    usr_wen_reg(0)		<= '0';
    if Data_wr_latch = '1' then
      Data_wr_reg		<= Data_wr;
      Byte_ena_reg	        <= Byte_ena;
      usr_wen_reg(0)	        <= '1';
    end if;
  end if;
end process;

usr_wen		<= usr_wen_reg(3);
usr_data_wr	<= Data_wr_reg;

	
	
decode_WR:pcie_decoder 
  port MAP
  (
    ADDR	   	=>	Addr_wr(16 downto 3), 
    BA			=>	'1', 
    LD_ADDR		=>	Addr_wr_latch, 
    CLOCK		=>	usr_clk_cell, 
    RESET		=>	usr_rst_n_cell, 
    -- Output ports
    SEL_func	        =>	usr_func_wr_cell				 	  
    );
 
decode_rd:pcie_decoder 
  port MAP
  (
    ADDR   		=>	Addr_rd(16 downto 3), 
    BA			=>	'1', 
    LD_ADDR		=>	Addr_rd_latch, 
    CLOCK		=>	usr_clk_cell, 
    RESET		=>	usr_rst_n_cell, 
    -- Output ports
    SEL_func	        =>	usr_func_rd_cell				 	  
    );
	
usr_func_wr	<= 	usr_func_wr_cell;
usr_func_rd	<=	usr_func_rd_cell;
		
process(usr_clk_cell)
begin
  if rising_edge(usr_clk_cell) then
    usr_rden_reg(3 downto 1) 	<= usr_rden_reg(2 downto 0);
    usr_rden_reg(0)		<= '0';
    if Addr_rd_latch = '1' then
      rID_rg			<= rID;
      usr_rden_reg(0)		<= '1';
    end if;
    
    data_rd_valid_rg		<= '0';
    if usr_rd_val = '1' then
      data_rd_valid_rg		<= '1';
      data_read_rg		<= usr_data_rd;
    end if;
  end if;
end process;	
	
usr_rden <= usr_rden_reg(3);

end Behavioral;
