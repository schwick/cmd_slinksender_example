library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;

                  
entity Counter is
  port (
    clk   : in std_logic;
    data  : out std_logic_vector( 63 downto 0 );
    cnten : in std_logic;
    reset : in std_logic
    );

end Counter;

architecture behavioral of Counter is

  signal cnt : std_logic_vector( 63 downto 0);

begin
  
  cntproc: process( clk, cnten, reset )

  begin  
    if reset = '1' then
        cnt <= (others => '0');
    elsif (rising_edge( clk ) and cnten ='1' )then
      cnt <= cnt + 1;    
    end if;
  end process;

  data <= cnt;
  
end behavioral;
  
                  
