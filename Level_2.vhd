----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2019 12:26:00 PM
-- Design Name: 
-- Module Name: Level_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all; 
use WORK.top_address_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


entity Level_2 is
    Port ( 
        GPIO_SW_N         : in std_logic;
        u250mhz_clk1_n    : in std_logic;
        u250mhz_clk1_p    : in std_logic;
        pcie_clk          : in std_logic;
        qpll_ref_clock_n  : in std_logic;
        qpll_ref_clock_p  : in std_logic;
        qsfp1_tx1_n       : out std_logic;
        qsfp1_tx1_p       : out std_logic;
        qsfp1_rx1_n       : in std_logic;
        qsfp1_rx1_p       : in std_logic;
        
        qsfp2_tx1_n       : out std_logic;
        qsfp2_tx1_p       : out std_logic;
        qsfp2_rx1_n       : in std_logic;
        qsfp2_rx1_p       : in std_logic;

        usr_func_wr       : in std_logic_vector(16383 downto 0);
        usr_wen           : in std_logic;
        usr_data_wr       : in std_logic_vector(63 downto 0);
        usr_func_rd       : in std_logic_vector(16383 downto 0);
        usr_rden          : in std_logic;
        read_data         : out std_logic_vector(63 downto 0);

        lff_in            : in std_logic;

        out_clk           : out std_logic;
        link_down         : out std_logic;
        link_full         : out std_logic;

        Rst_hrd_sim       : in std_logic
        );
end Level_2;


--------------------------------------------------------------------------------

architecture Behavioral of Level_2 is

-------------------------------------------- Components ---------------------
  
component clk_wiz_0
port (
    clk_out1          : out    std_logic;
    clk_out2          : out    std_logic;
    reset             : in     std_logic;
    clk_in1_p         : in     std_logic;
    clk_in1_n         : in     std_logic
    );
end component;

component FED_fragment_CRC16_D128b is
  port (
    clk         : in std_logic;
    clear_p     : in std_logic;
    Data        : in std_logic_vector( 127 downto 0 );
    enable      : in std_logic;
    CRC_out     : out std_logic_vector( 15 downto 0 )
    );
end component;


COMPONENT SR_sender_GLB_0
  PORT (
    aresetn              : IN STD_LOGIC;
    txdiffctrl_in        : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    txpostcursor_in      : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    txprecursor_in       : IN STD_LOGIC_VECTOR(4 DOWNTO 0);

    Core_status_addr     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    Core_status_data_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    user_100MHz_clk      : IN STD_LOGIC;
    FED_CLOCK            : IN STD_LOGIC;
    event_data_word      : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    event_ctrl           : IN STD_LOGIC;
    event_data_wen       : IN STD_LOGIC;
    backpressure         : OUT STD_LOGIC;
    Link_DOWN_n          : OUT STD_LOGIC;
    ext_trigger          : IN STD_LOGIC;
    ext_veto_out         : OUT STD_LOGIC;
    qpll_lock_in         : IN STD_LOGIC;
    qpll_reset_out       : OUT STD_LOGIC;
    qpll_clkin           : IN STD_LOGIC;
    qpll_ref_clkin       : IN STD_LOGIC;
    Snd_gtyrxn_in        : IN STD_LOGIC;
    Snd_gtyrxp_in        : IN STD_LOGIC;
    Snd_gtytxn_out       : OUT STD_LOGIC;
    Snd_gtytxp_out       : OUT STD_LOGIC;
    Rst_hrd_sim		 : IN STD_LOGIC

  );
END COMPONENT;


component QPLL_wrapper_SR25_GTY_ref15625
	Port ( 
          gtrefclk00_in				: in std_logic;
          gtrefclk01_in       		: in std_logic;
          qpll0reset_in       		: in std_logic;
          qpll1reset_in       		: in std_logic;
          
          qpll0lock_in        		: out std_logic;
          qpll0outclk_out     		: out std_logic;
          qpll0outrefclk_out  		: out std_logic;
									
          qpll1lock_in        		: out std_logic;
          qpll1outclk_out     		: out std_logic;
          qpll1outrefclk_out  		: out std_logic
          );
end component;


-------------- for testing with a receiver -------------------------------------
component Counter
  port(
    clk   : in std_logic;
    data  : out std_logic_vector( 63 downto 0 );
    cnten : in std_logic;
    --rden  : in std_logic;
    reset : in std_logic
    );
end component;

COMPONENT SR_Receive_GLB_0
  PORT (
    usr_clk           : IN STD_LOGIC;
    rst_usr_clk_n     : IN STD_LOGIC;
    usr_func_wr       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_wen           : IN STD_LOGIC;
    usr_data_wr       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);

    usr_func_rd       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
    usr_rden          : IN STD_LOGIC;
    usr_dto_receiver  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    
    user_100MHz_clk   : IN STD_LOGIC;
    
    clk_out           : OUT STD_LOGIC;
    wen_data          : OUT STD_LOGIC;
    UCTRL_out         : OUT STD_LOGIC;
    data_out          : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    lff               : IN STD_LOGIC;

    ena_cnt_pckt      : OUT STD_LOGIC;
    ena_cnt_bad_p     : OUT STD_LOGIC;
    ena_back_pres     : OUT STD_LOGIC;
    ena_FED_crc_err   : OUT STD_LOGIC;
    ena_SLINK_crc_err : OUT STD_LOGIC;

    qpll_lock_in      : IN STD_LOGIC;
    qpll_reset_out    : OUT STD_LOGIC;
    qpll_clkin        : IN STD_LOGIC;
    qpll_ref_clkin    : IN STD_LOGIC;

    Rcv_gtyrxn_in     : IN STD_LOGIC;
    Rcv_gtyrxp_in     : IN STD_LOGIC;
    Rcv_gtytxn_out    : OUT STD_LOGIC;
    Rcv_gtytxp_out    : OUT STD_LOGIC

  );
END COMPONENT;


-------------------------------- Signals -----------------------------------------

-- basic
signal clk              : std_logic;
signal reset            : std_logic;
signal reset_n          : std_logic;
signal resetCounter     : std_logic := '0';

-- counters
-- counters to generate payload: the low one counts always the high one only
-- when data can be written. Hence it can be used to measure the backpressure.
signal count_low      : std_logic_vector ( 63 downto 0 );
signal count_high     : std_logic_vector ( 63 downto 0 );

signal incr_high      : std_logic; --increment payload counter (MSW)

-- event counter
signal evtcnt         : std_logic_vector( 31 downto 0 );
signal incr_evtcnt    :  std_logic;

-- counts the size of the event (backwards to 0)
-- evtSize holds a hardcoded event size
signal sizecnt        : std_logic_vector( 31 downto 4 );
signal evtSize        : std_logic_vector( 31 downto 4 );
signal decr_sizecnt   : std_logic;

-- Slink interface
signal event_data       : std_logic_vector( 127 downto 0);
signal event_data_1     : std_logic_vector( 127 downto 0);
signal event_data_2     : std_logic_vector( 127 downto 0);
signal event_ctrl       : std_logic;
signal event_ctrl_1     : std_logic;
signal event_ctrl_2     : std_logic;
signal event_trailer    : std_logic;
signal event_data_wen   : std_logic;
signal event_data_wen_1 : std_logic;
signal event_data_wen_2 : std_logic;
signal event_trailer_1  : std_logic;
signal lff              : std_logic;
signal link_down_n      : std_logic;

-- event generator state-machine
type evgen_state_type is (s_idle, s_header, s_payload, s_trailer, s_reset);
signal evgen_state, next_evgen_state : evgen_state_type;
signal canwrite       : std_logic;
signal ena_payload    : std_logic;
signal ena_header     : std_logic;
signal ena_trailer    : std_logic;
signal ena_crc        : std_logic;
signal reset_crc      : std_logic;
signal reset_crc_1    : std_logic;
--signal reset_crc_2    : std_logic;
signal rcrc           : std_logic;

-- holds the crc to be multiplexed on data bus when trailer is transferred
signal crc_value      : std_logic_vector( 15 downto 0 );

-- connects the QPLL wrapper clock signals to the sender
signal qpll_reset_sender : std_logic;
signal qpll_clk       : std_logic;
signal qpll_ref_clk   : std_logic;
signal qpll_lock      : std_logic;

-- connects the QPLL wrapper clock signals to the receiver
signal qpll_reset_receiver : std_logic;
signal qpll_clk_r       : std_logic;
signal qpll_ref_clk_r   : std_logic;
signal qpll_lock_r      : std_logic;

-- output of clock buffer for transceiver
signal gt_ref_clk     : std_logic;
signal qpll_reset     : std_logic;

-- for the receiver part
signal receiver_data     : std_logic_vector( 63 downto 0 );
signal fed_clk_out       : std_logic;
signal wen_data          : std_logic;
signal UCTRL_out         : std_logic;
signal data_out          : std_logic_vector( 127 downto 0 );
signal ena_cnt_pckt      : std_logic;
signal ena_cnt_bad       : std_logic;
signal ena_cnt_backpr    : std_logic;
signal ena_cnt_fedcrc    : std_logic;
signal ena_cnt_slinkcrc  : std_logic;

-- enable signals for reading out the counters
signal pckt_cnt          : std_logic_vector( 63 downto 0 );
signal bad_cnt           : std_logic_vector( 63 downto 0 );
signal backpr_cnt        : std_logic_vector( 63 downto 0 );
signal fedcrc_cnt        : std_logic_vector( 63 downto 0 );
signal slinkcrc_cnt      : std_logic_vector( 63 downto 0 );

signal ena_rd_pckt       : std_logic;
signal ena_rd_bad        : std_logic;
signal ena_rd_backpr     : std_logic;
signal ena_rd_fedcrc     : std_logic;
signal ena_rd_slinkcrc   : std_logic;

signal core_status_data_out : std_logic_vector( 63 downto 0 );
signal status_adr           : std_logic_vector( 15 downto 0 );
begin

clockdriver : clk_wiz_0
   port map ( 
     -- Clock in ports
     clk_in1_p => u250mhz_clk1_p,
     clk_in1_n => u250mhz_clk1_n,
     -- Clock out ports  
     clk_out1 => clk,         -- 100MHz
     -- clk_out2 => clk_out2,    -- 150MHz not used
     reset     => '0'
     );

slinkSender : SR_sender_GLB_0
  PORT MAP (
    aresetn           => reset_n,
    txdiffctrl_in     => "11000",
    txpostcursor_in   => "10100",
    txprecursor_in    => "00000",
    
    Core_status_addr => status_adr,
    Core_status_data_out => core_status_data_out,

    user_100MHz_clk => clk,
    FED_CLOCK       => clk,
    event_data_word => event_data_2,
    event_ctrl      => event_ctrl_2,
    event_data_wen  => event_data_wen_2,
    backpressure    => lff,
    Link_DOWN_n     => link_down_n, -- active low!
    ext_trigger     => '0',
    --ext_veto_out => ext_veto_out,

    qpll_lock_in    => qpll_lock,
    qpll_reset_out  => qpll_reset_sender,
    qpll_clkin      => qpll_clk,
    qpll_ref_clkin  => qpll_ref_clk,
    
    Snd_gtyrxn_in   => qsfp1_rx1_n,
    Snd_gtyrxp_in   => qsfp1_rx1_p,
    Snd_gtytxn_out  => qsfp1_tx1_n,
    Snd_gtytxp_out  => qsfp1_tx1_p,
    Rst_hrd_sim		=> Rst_hrd_sim

    );

link_down <= not link_down_n;
link_full <= lff;

--**************************     Ref clock buffer      **************************************  

IBUFDS_GTE4_inst : IBUFDS_GTE4 
port map (
	O     	=> gt_ref_clk, -- 1-bit output: Refer to Transceiver User Guide
	--ODIV2 => ODIV2, -- 1-bit output: Refer to Transceiver User Guide
	CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
	I     	=> qpll_ref_clock_p, -- 1-bit input: Refer to Transceiver User Guide 
	IB      => qpll_ref_clock_n -- 1-bit input: Refer to Transceiver User Guide
);  

-- For simulation to be commented out
qpll_reset <= '1' WHEN qpll_reset_sender = '1'  or qpll_reset_receiver = '1'  else '0'; 


qpll_I1:QPLL_wrapper_SR25_GTY_ref15625
	Port MAP( 
          gtrefclk00_in		        => gt_ref_clk,
          gtrefclk01_in       		=> '0',
          qpll0reset_in       		=> qpll_reset,
          qpll1reset_in       		=> reset,
          
          qpll0lock_in        		=> qpll_lock,
          qpll0outclk_out     		=> qpll_clk,
          qpll0outrefclk_out  		=> qpll_ref_clk
          
--          qpll1lock_in        		=> qpll_lock_r,
--          qpll1outclk_out     		=> qpll_clk_r,
--          qpll1outrefclk_out  		=> qpll_ref_clk_r
          );

qpll_I1_r:QPLL_wrapper_SR25_GTY_ref15625
	Port MAP( 
          gtrefclk00_in		        => gt_ref_clk,
          gtrefclk01_in       		=> '0',
          qpll0reset_in       		=> qpll_reset,
          qpll1reset_in       		=> reset,
          
          qpll0lock_in        		=> qpll_lock_r,
          qpll0outclk_out     		=> qpll_clk_r,
          qpll0outrefclk_out  		=> qpll_ref_clk_r
          
--          qpll1lock_in        		=> qpll_lock_r,
--          qpll1outclk_out     		=> qpll_clk_r,
--          qpll1outrefclk_out  		=> qpll_ref_clk_r
          );

------------ make clock available outside (for simulation) --------------

out_clk <= clk;

-------------------------- Sender part -----------------------------------
rcrc <= reset_crc_1 or reset;
slinkcrc_i:FED_fragment_CRC16_D128b
    port map ( 
        clear_p  => rcrc,
        clk      => clk,
        enable   => ena_crc,
        Data     => event_data( 127 downto 0 ),
        CRC_out  => crc_value
    );

  
-- some preliminary signal fixing
    process( clk ) begin
    if rising_edge(clk) then
        canwrite <= (not lff and link_down_n);
        end if;
        end process;
        
  evtSize <= "0000000000000000000000001000"; 
  reset <= GPIO_SW_N;
  reset_n <= not( GPIO_SW_N );
  
-- Counter for higher 64 bits of payload:  only count the clocks which are really written to the slink.
    process ( clk ) begin
        if rising_edge(clk) then
            if ( reset = '1' ) then
                count_high <= (others => '0' );
            else
                if ( incr_high = '1' ) then
                    count_high <= count_high + '1';
                end if;
            end if;
        end if;
    end process;

-- Counter for lower 64 bits of payload: count all clocks    
    process ( clk ) begin
        if rising_edge(clk) then
            if ( reset = '1' ) then
                count_low <= (others=>'0');
            else
                count_low <= count_low + '1';
            end if;
        end if;
    end process;

-- Event counter
    process ( clk ) begin
        if (rising_edge(clk)) then
            if ( reset = '1' ) then
                evtcnt <= std_logic_vector(to_unsigned(1, evtcnt'length));
            else
                if ( incr_evtcnt = '1' ) then
                    evtcnt <= evtcnt + '1';
                end if;
            end if;
        end if;
    end process;

-- Event Size counter : counts backwards

    process( clk ) begin
        if (rising_edge(clk)) then
            if( reset = '1' ) then
                sizecnt <= evtSize;
            elsif( sizecnt = 0 ) then
                sizecnt <= evtSize;
            elsif( decr_sizecnt = '1' ) then
                sizecnt <= sizecnt - '1';
            end if;
            if ( sizecnt = 1 ) then
            end if;
        end if;
    end process;
    
 ------------------- State machine for the event generator -------------------------------
    evgen_sync : process (clk)
    begin
        if rising_edge(clk) then
            if ( reset = '1' ) then
                evgen_state <= s_idle;
            else
                evgen_state <= next_evgen_state;
            end if;
        end if;
    end process;
    
next_state_decode : process( evgen_state, canwrite, sizecnt )
begin
  next_evgen_state <= evgen_state;
  case (evgen_state) is 
    when s_idle =>
      if (canwrite = '1') then
        next_evgen_state <= s_header;
      end if;
    when ( s_header ) =>
      if ( canwrite = '1' ) then
        next_evgen_state <= s_payload;
      end if;
    when ( s_payload ) =>
      if ( canwrite = '1' and sizecnt = 1 ) then
        next_evgen_state <= s_trailer;
      end if;
    when ( s_trailer ) =>
      if ( canwrite = '1' ) then
        next_evgen_state <= s_reset;
      end if;
    when ( s_reset ) =>
      next_evgen_state <= s_idle;
  end case;
end process;

evtgen_output : process(evgen_state, canwrite)
begin
  ena_header <= '0';
  ena_trailer <= '0';
  ena_crc <= '0';
  reset_crc <= '0';
  ena_payload <= '0';
  
  case( evgen_state ) is

    when( s_idle ) =>
      reset_crc <= '0';
    
    when( s_header ) =>
      if ( canwrite = '1' ) then
        ena_header <= '1';
        ena_crc <= '1';
      end if;
    
    when( s_trailer ) =>
      if ( canwrite = '1' ) then
        ena_trailer <= '1';
        ena_crc <= '1';
      end if;

      when( s_reset ) =>
        reset_crc <= '1';
    
    when( s_payload ) =>
      if ( canwrite = '1' ) then
        ena_payload <= '1';
        ena_crc <= '1';
      end if; 
  end case;
  end process;


  -- Multiplexer on the event_data bus
  evtdata_mux : process( ena_header, ena_trailer, ena_payload, evtcnt, sizecnt, crc_value, count_high, count_low )
  begin
    event_data <= ( others => '0' );
    event_ctrl <= '0';
    event_trailer <= '0';
    incr_evtcnt <= '0';
    decr_sizecnt <= '0';
    event_data_wen <= '0';
    incr_high <= '0';
    if ( ena_header = '1' ) then
      event_data( 127 downto 120 ) <= x"55";
      event_data( 119 downto 096 ) <= x"000000";
      event_data( 095 downto 064 ) <= evtcnt;
      event_data( 063 downto 032 ) <= x"00000123";
      event_data( 031 downto 000 ) <= x"deadface";
      event_ctrl <= '1';
      event_data_wen <= '1';
    elsif ( ena_trailer = '1' ) then
      event_data( 127 downto 120 ) <= x"AA";
      event_data( 119 downto 092 ) <= x"0000000";
      event_data( 091 downto 064 ) <= sizecnt;
      event_data( 063 downto 048 ) <= x"0000";
      event_data( 047 downto 000 ) <= x"000000000000";
      event_ctrl <= '1';
      event_trailer <= '1';
      incr_evtcnt <= '1';
      event_data_wen <= '1';
    elsif ( ena_payload = '1' ) then
      event_data( 127 downto 64 ) <= count_high;
      event_data(  63 downto  0 ) <= count_low;
      event_data_wen <= '1';
      incr_high <= '1';
      decr_sizecnt <= '1';
    end if;      
  end process;

  -- Register to compose the final data and to multiplex the crc in
  crcmultiplex : process( event_trailer, event_data( 063 downto 048 ), clk )
  begin
    if rising_edge( clk ) then
      
      event_ctrl_1 <= event_ctrl;
      event_trailer_1 <= event_trailer;
      event_data_wen_1 <= event_data_wen;
      event_ctrl_2 <= event_ctrl_1;
      event_data_wen_2 <= event_data_wen_1;
      event_data_1 <= event_data;

      reset_crc_1 <= reset_crc;
--      reset_crc_2 <= reset_crc;
      event_data_2( 127 downto 64 ) <= event_data_1( 127 downto 64 );
      event_data_2( 47 downto 0 ) <= event_data_1( 47 downto 0 );
      if event_trailer_1 = '1' then
        event_data_2( 63 downto 48 ) <= crc_value;
      else
        event_data_2( 63 downto 48 ) <= event_data( 63 downto 48 );
      end if;
        
    end if;
  end process;
    

------------------------- Receiver part ----------------------------

  
  slink_receiver : SR_Receive_GLB_0
    PORT MAP (
      usr_clk           => pcie_clk,
      rst_usr_clk_n     => reset_n,
      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,
      usr_data_wr       => usr_data_wr,

      usr_func_rd       => usr_func_rd,
      usr_rden          => usr_rden,
      usr_dto_receiver  => receiver_data,

      user_100MHz_clk   => clk,

      clk_out           => fed_clk_out,
      wen_data          => wen_data,
      UCTRL_out         => UCTRL_out,
      data_out          => data_out,
      lff               => lff_in,

      ena_cnt_pckt      => ena_cnt_pckt,
      ena_cnt_bad_p     => ena_cnt_bad,
      ena_back_pres     => ena_cnt_backpr,
      ena_FED_crc_err   => ena_cnt_fedcrc,
      ena_SLINK_crc_err => ena_cnt_slinkcrc,

      qpll_lock_in      => qpll_lock_r,
      qpll_reset_out    => qpll_reset_receiver,
      qpll_clkin        => qpll_clk_r,
      qpll_ref_clkin    => qpll_ref_clk_r,

      Rcv_gtyrxn_in     => qsfp2_rx1_n,
      Rcv_gtyrxp_in     => qsfp2_rx1_p,
      Rcv_gtytxn_out    => qsfp2_tx1_n,
      Rcv_gtytxp_out    => qsfp2_tx1_p
  );

  -- connect various counters which can be read out

  -- generate the resetCounter signal
  resetCntProc : process( clk, usr_wen )

  begin  
    if rising_edge( clk ) then
      if ( usr_func_wr( adr_reset_cnt ) = '1' and usr_wen = '1' ) then
        resetCounter <= '1';
      else
        resetCounter <= '0';
      end if;
    end if;  

  end process;
    
  -- packet counter
  ena_rd_pckt <= usr_func_rd(adr_packet_counter);
  pcktCounter : Counter
    port map (
      clk   => fed_clk_out,
      data  => pckt_cnt,
      cnten => ena_cnt_pckt,
      --rden  => ena_rd_pckt,
      reset => resetCounter
      );

  -- bad counter  
  ena_rd_bad <= usr_func_rd(adr_bad_counter);
  badCounter : Counter
    port map (
      clk   => fed_clk_out,
      data  => bad_cnt,
      cnten => ena_cnt_bad,
      --rden  => ena_rd_bad,
      reset => resetCounter
      );

  -- backpressure counter  
  ena_rd_backpr <= usr_func_rd(adr_backpr_counter);
  backprCounter : Counter
    port map (
      clk   => fed_clk_out,
      data  => backpr_cnt,
      cnten => ena_cnt_backpr,
      --rden  => ena_rd_backpr,
      reset => resetCounter
      );

  -- fed crc error counter  
  ena_rd_fedcrc <= usr_func_rd(adr_fedcrc_counter);
  fedcrcCounter : Counter
    port map (
      clk   => fed_clk_out,
      data  => fedcrc_cnt,
      cnten => ena_cnt_fedcrc,
      --rden  => ena_rd_fedcrc,
      reset => resetCounter
      );

  -- slink crc error counter  
  ena_rd_slinkcrc <= usr_func_rd(adr_slinkcrc_counter);
  slinkcrcCounter : Counter
    port map (
      clk   => fed_clk_out,
      data  => slinkcrc_cnt,
      cnten => ena_cnt_slinkcrc,
      --rden  => ena_rd_slinkcrc,
      reset => resetCounter
      );

  rd_mplx : process( usr_func_rd, pckt_cnt, bad_cnt, backpr_cnt, fedcrc_cnt, slinkcrc_cnt, receiver_data, core_status_data_out )
  begin

    if usr_func_rd( adr_packet_counter) = '1' then
      read_data <= pckt_cnt;
    elsif usr_func_rd( adr_bad_counter ) = '1' then
      read_data <= bad_cnt;
    elsif usr_func_rd( adr_backpr_counter ) = '1' then
      read_data <= backpr_cnt;
    elsif usr_func_rd( adr_fedcrc_counter ) = '1' then
      read_data <= fedcrc_cnt;
    elsif usr_func_rd( adr_slinkcrc_counter ) = '1' then
      read_data <= slinkcrc_cnt;
    elsif usr_func_rd( adr_sender_status ) = '1' then
      read_data <= core_status_data_out;
    else
      read_data <= receiver_data;
    end if;

  end process;


  -------------------- register for addressig the status words of the Slink Sender -----------------------
  --        The status information appears at the output of the slink sender
  --        core when the address is set. The reading of the status work is
  --        included in the read process above. 

  statusAdrReg : process( clk, usr_func_wr, usr_wen )
  begin
    if ( rising_edge(clk) and usr_wen='1' and  usr_func_wr( adr_sender_status ) = '1' ) then
      status_adr <= usr_data_wr( 15 downto 0 );
    end if;
  end process;
 
end Behavioral;
