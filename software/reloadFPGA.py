#!/usr/bin/python

import sys
import os
import commands
import time
import hal 

 
list_arg 	= sys.argv 
 
dth 		= hal.PCIDevice("addresstable.dat", 0xecd6, 0xfea1, 0, False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
dth.write( "Bit_CTRL", 1 )

Conf_val = ['0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'] 
 
####################################################################################################
print "------read Configuration ------"
#read 16 config words
for x in range (0,16):
	offset = int(x) * 4
	Conf_val[x] 	= dth.read("ConfigStart",offset)
	print hex(Conf_val[x])
	
print "You can program the FPGA ....."

wait = raw_input("Hi ENTER when prog is DONE!")

for x in range (0,16):
	offset = int(x) * 4
	dth.write("ConfigStart",Conf_val[x],False,offset) 
 
#dth.write("Reset_glb",0x20000,False,offset) 
print "DONE"
 
