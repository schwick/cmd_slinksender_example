#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
import os
from time import sleep

from  array import array   # arrays are needed for block transfers
2

############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item, offset = 0 ):
    val = dev.read64(item, offset = offset)
    print( "{:30s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

def printlist( dev, itemlist ):
    print( '' )
    for it in itemlist:
        val = dev.read64(it)
        print( "{:30s} ==> 0x{:016x} (dec:{})".format( it, val, val ))
    print( '' )

def senderStatus( dev, item, adr ):
    dev.write( 'sender_status_address', adr )
    val = dev.read64( 'sender_status_address')
    print( "{:35s} {} ==> 0x{:016x} (dec:{})".format(item, adr, val, val ))
    return val

def decodeBits( table, word ):
    for i in table:
        mask = i[0]
        val = word & mask
        while mask & 1 == 0:
            mask = mask >> 1
            val = val >>1
        txt = i[1].replace("\n","\n                        ")
        print( "0x{:08x} (dec:{:4d}) : {:s}".format(val,val,txt))
    print("")

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
tb = hal.PCIDevice( "addresstable.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )

while True:
    os.system('clear')
    print("Receiver status")
    print("===============")
    rprint(tb,"SR_RecSERDES_DRP_ct")
    rprint(tb,"SR_RecSERDES_DRP_rtn")
    rprint(tb,"SR_RecSERDES_status")
    rprint(tb,"SR_RecSERDES_setup")
    print("")

    print("Detailed status bits for the receiver")
    print("=====================================")

    itemlist = [
        "recSerdesInitDone",
        "recSerdesInitRetries",    
        "recSerdesSyncShifting", 
        "recSerdesLoopback", 
        "recSerdesTxPolarity",   
        "recSerdesRxPolarity"
    ]

    printlist( tb, itemlist )

    itemlist  = [
        "rxpmaresetdone",               
        "rxprgdivresetdone",             
        "txpmaresetdone",             
        "txprgdivresetdone",             
        "rxresetdone",                   
        "qpll_reset_cell",               
        "gtpowergood_out",               
        "reset_rx_done",                 
        "reset_tx_done",                 
        "rx_clock_ready",                
        "tx_clock_ready",                
        "qpll_lock_in",                  
        "STATE_link",                  
        "Link_locked",                   
        "ST_START",                      
        "ST_check_pattern",              
        "ST_slip_state",                 
        "ST_wait_slip_done",             
        "ST_check_swap_word",            
        "ST_send_idle",                  
        "ST_wait_idle_from_other_side",  
        "ST_link_up"
    ]                   
    
    printlist( tb, itemlist )
    sleep(0.5)
