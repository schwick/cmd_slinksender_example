#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
from time import sleep

from  array import array   # arrays are needed for block transfers

def printusage():
    print( "Usage : evtgen.py  [1|0]  (1 to start, 0 to stop)" )
    sys.exit()

############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item, offset = 0 ):
    val = dev.read64(item, offset = offset)
    print( "{:25s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

def printlist( dev, itemlist ):
    print( '' )
    for it in itemlist:
        val = dev.read64(it)
        print( "{:25s} ==> 0x{:016x} (dec:{})".format( it, val, val ))
    print( '' )

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
tb = hal.PCIDevice( "addresstable.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )


if len(sys.argv) == 2:
    start = int(sys.argv[1])
    if start != 1 and start != 0:
        printusage()
else :
    printusage()

tb.write64( "evgen_run", start)
