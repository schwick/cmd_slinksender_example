#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
import os
from time import sleep

from  array import array   # arrays are needed for block transfers


############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item, offset = 0 ):
    val = dev.read64(item, offset = offset)
    print( "{:30s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

def printlist( dev, itemlist ):
    print( '' )
    for it in itemlist:
        val = dev.read64(it)
        print( "{:30s} ==> 0x{:016x} (dec:{})".format( it, val, val ))
    print( '' )

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
tb = hal.PCIDevice( "addresstable.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )

offset = 0
if len(sys.argv) == 2:
    channel = int(sys.argv[1])
    offset = channel * 0x80

# read out the packet counter
#tb.write( "reset_cnt", 1 )

while True:
    os.system('clear')
    rprint( tb, "packet_counter", offset )
    rprint( tb, "bad_counter", offset )
    rprint( tb, "backpr_counter", offset )
    rprint( tb, "fedcrc_counter", offset )
    rprint( tb, "slinkcrc_counter", offset )
    sleep(1)

# itemlist = [
#     "recSerdesInitDone",
#     "recSerdesInitRetries",    
#     "recSerdesSyncShifting", 
#     "recSerdesLoopback", 
#     "recSerdesTxPolarity",   
#     "recSerdesRxPolarity"
# ]
# 
# printlist( tb, itemlist )
# 
# itemlist  = [
#     "rxpmaresetdone",               
#     "rxprgdivresetdone",             
#     "txpmaresetdone",             
#     "txprgdivresetdone",             
#     "rxresetdone",                   
#     "qpll_reset_cell",               
#     "gtpowergood_out",               
#     "reset_rx_done",                 
#     "reset_tx_done",                 
#     "rx_clock_ready",                
#     "tx_clock_ready",                
#     "qpll_lock_in",                  
#     "STATE_link",                  
#     "Link_locked",                   
#     "ST_START",                      
#     "ST_check_pattern",              
#     "ST_slip_state",                 
#     "ST_wait_slip_done",             
#     "ST_check_swap_word",            
#     "ST_send_idle",                  
#     "ST_wait_idle_from_other_side",  
#     "ST_link_up"
# ]                   
# 
# 
# printlist( tb, itemlist )
# 
# 
# rprint(tb, "SR_RecSERDES_reset")
# rprint(tb, "SR_RecSERDES_DRP_ct")
# rprint(tb, "SR_RecSERDES_DRP_rtn")
# rprint(tb, "SR_RecSERDES_status")
# rprint(tb, "SR_RecSERDES_setup")
