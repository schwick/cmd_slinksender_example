#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
import os
from time import sleep

from  array import array   # arrays are needed for block transfers
2

############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item, offset = 0 ):
    val = dev.read64(item, offset = offset)
    print( "{:30s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

def printlist( dev, itemlist ):
    print( '' )
    for it in itemlist:
        val = dev.read64(it)
        print( "{:30s} ==> 0x{:016x} (dec:{})".format( it, val, val ))
    print( '' )

def senderStatus( dev, item, adr ):
    dev.write( 'sender_status_address', adr )
    val = dev.read64( 'sender_status_address')
    print( "{:35s} {} ==> 0x{:016x} (dec:{})".format(item, adr, val, val ))
    return val

def decodeBits( table, word ):
    for i in table:
        mask = i[0]
        val = word & mask
        while mask & 1 == 0:
            mask = mask >> 1
            val = val >>1
        txt = i[1].replace("\n","\n                        ")
        print( "0x{:08x} (dec:{:4d}) : {:s}".format(val,val,txt))
    print("")

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
tb = hal.PCIDevice( "addresstable.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )

print("Sender status")
print("=============")

# internal status:
# bit     meaning
table = [
    [0x1        ,"Statemachine Block reader: bit 0 ST_START"],
    [0x2        ,"Statemachine Block reader: bit 1 READ_FIFO"],
    [0x4        ,"Statemachine Block reader: bit 2 BLOCK_END : block has been filled and crc is being inserted."],
    [0x8        ,"event being processed"],
    [0x10       ,"Header found without preceeding Trailer"],
    [0x20       ,"Trailer found without preceeding Header"],
    [0x40       ,"found duplicated event number (2 equal event numbers in subsequent events)"],
    [0x10000000 ,"1 when a free block is available"],
    [0x20000000 ,"backpressure sent to FED. This is the Almost Full flag of the FIFO_sync.\nIt is activated when the word count reaches 48 (out of 64). I.e. 16 words can still be written."],
    [0x40000000 ,"LINKDOWN_n (Essentially the DAQ ON/OFF state)"],
    [0x80000000 ,"Test_mode "],
]


val = senderStatus( tb, "Internal Status", 1 )
decodeBits( table, val )


senderStatus( tb, "Data Counter", 2 )
senderStatus( tb, "Event Counter", 3 )
senderStatus( tb, "Block Counter", 4 )
senderStatus( tb, "Received Packet Counter", 5 )
senderStatus( tb, "State of Core", 6 )
senderStatus( tb, "Sent Packet Counter", 7 )

# One hot state machine:
# bit   meaning
#  0    ground state
#  1    start of frame
#  4    data filling
#  5    Fill the CRC
#  7    Gap between packets
val = senderStatus( tb, "State of packet Builder", 8 )
table = [
    [ 0x01, "Ground State"],
    [ 0x02, "Start of Frame"],
    [ 0x10, "Data writing"],
    [ 0x20, "CRC insertion"],
    [ 0x80, "Inter Packet Gap"]
]
decodeBits(table, val)
senderStatus( tb, "Backpressure Counter", 9 )
senderStatus( tb, "Version", 10 )
senderStatus( tb, "Status of Serdes", 11 )
senderStatus( tb, "Retransmit Counter", 12 )
senderStatus( tb, "FED CRC Error Counter",13 )
# for 1 ms the FED clock is counted:
senderStatus( tb, "FED Clock Frequency Measurement", 14 )

print("")
print("Receiver status")
print("===============")
rprint(tb,"SR_RecSERDES_DRP_ct")
rprint(tb,"SR_RecSERDES_DRP_rtn")
rprint(tb,"SR_RecSERDES_status")
rprint(tb,"SR_RecSERDES_setup")
print("")

print("Detailed status bits for the receiver")
print("=====================================")

itemlist = [
    "recSerdesInitDone",
    "recSerdesInitRetries",    
    "recSerdesSyncShifting", 
    "recSerdesLoopback", 
    "recSerdesTxPolarity",   
    "recSerdesRxPolarity"
]

printlist( tb, itemlist )

itemlist  = [
    "rxpmaresetdone",               
    "rxprgdivresetdone",             
    "txpmaresetdone",             
    "txprgdivresetdone",             
    "rxresetdone",                   
    "qpll_reset_cell",               
    "gtpowergood_out",               
    "reset_rx_done",                 
    "reset_tx_done",                 
    "rx_clock_ready",                
    "tx_clock_ready",                
    "qpll_lock_in",                  
    "STATE_link",                  
    "Link_locked",                   
    "ST_START",                      
    "ST_check_pattern",              
    "ST_slip_state",                 
    "ST_wait_slip_done",             
    "ST_check_swap_word",            
    "ST_send_idle",                  
    "ST_wait_idle_from_other_side",  
    "ST_link_up"
]                   

printlist( tb, itemlist )
