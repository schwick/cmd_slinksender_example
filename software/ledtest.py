#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
from time import sleep

from  array import array   # arrays are needed for block transfers


############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item ):
    val = dev.read(item)
    print( "{:20s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
testboard = hal.PCIDevice( "addresstable.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )

rprint( testboard, "Vendor_Id" )
rprint( testboard, "Device_Id" )

rprint( testboard, "LED_test" )

# switch on led 0
testboard.write( "LED_test", 1 )
rprint( testboard, "LED_test" )
sleep(0.5)

# switch on led 0
testboard.write( "LED_test", 2 )
rprint( testboard, "LED_test" )
sleep(0.5)

testboard.write( "LED_test", 3 )
rprint( testboard, "LED_test" )
sleep(0.5)

testboard.write( "LED_test", 0 )


