#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
from time import sleep

from  array import array   # arrays are needed for block transfers


############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:016x}  (dec:{})".format(num,num))
#################################################################

def rprint( dev, item, offset = 0 ):
    val = dev.read64(item, offset = offset)
    print( "{:25s} ==> 0x{:016x} (dec:{})".format( item, val, val ))

def printlist( dev, itemlist ):
    print( '' )
    for it in itemlist:
        val = dev.read64(it)
        print( "{:25s} ==> 0x{:016x} (dec:{})".format( it, val, val ))
    print( '' )

#  Construct PCIDevices representing devices on the PCI bus
#                    AddressTablePath, Vendor, Device, Index, swapflag
tb = hal.PCIDevice( "addresstable_4c.dat",  0xecd6, 0xfea1, 0,     False )

# Just in case the BIOS does not do this: enable PCI memory access to the card
tb.write( "Bit_CTRL", 1 )

istart = 0
istop = 4
if len(sys.argv) == 2:
    channel = int(sys.argv[1])
    istart = channel
    istop = channel+1

for ic in range(istart,istop):
    print("daqoff of channel " + repr(ic+1) )
    tb.write64( "slinkrocket_access_command", 0x80010006, offset = ic*0x80 )
    tb.write64( "slinkrocket_access_data",    0x30000001, offset = ic*0x80 )


