LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.Numeric_std.all;

entity pcie_decoder is
  port
    (
      -- Input ports
      ADDR      : in  std_logic_vector(16 downto 3);
      BA        : in  std_logic;
      LD_ADDR   : in  std_logic;
      CLOCK     : in  std_logic;
      RESET     : in  std_logic;
      
      -- Output ports
      SEL_func  : out std_logic_vector(16383 downto 0)
      );
end pcie_decoder;


architecture behavioral of pcie_decoder is

  signal reg   : std_logic_vector(16383 downto 0); 
        
begin
        
  process(CLOCK,RESET,ADDR)

    variable address       : integer;

  begin

    address       := to_integer(unsigned(ADDR(15 downto  3))); -- 32-bit registers 
    
    if rising_edge(clock) then

      if (LD_ADDR = '1' and BA = '1') then
        
        reg                  <= (others => '0');
        reg(address)         <= '1';

      end if;

    end if;

  end process;

  SEL_func <= reg;

end behavioral;
