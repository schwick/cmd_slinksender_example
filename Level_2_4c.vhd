----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2019 12:26:00 PM
-- Design Name: 
-- Module Name: Level_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all; 
use WORK.top_address_constants.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


entity Level_2_4c is
  Port ( 
    GPIO_SW_N         : in std_logic;
    u250mhz_clk1_n    : in std_logic;
    u250mhz_clk1_p    : in std_logic;
    pcie_clk          : in std_logic;
    qpll_ref_clock_n  : in std_logic;
    qpll_ref_clock_p  : in std_logic;

    qsfp1_tx1_n_c0    : out std_logic;
    qsfp1_tx1_p_c0    : out std_logic;
    qsfp1_rx1_n_c0    : in std_logic;
    qsfp1_rx1_p_c0    : in std_logic;
               
    qsfp2_tx1_n_c0    : out std_logic;
    qsfp2_tx1_p_c0    : out std_logic;
    qsfp2_rx1_n_c0    : in std_logic;
    qsfp2_rx1_p_c0    : in std_logic;

    qsfp1_tx1_n_c1    : out std_logic;
    qsfp1_tx1_p_c1    : out std_logic;
    qsfp1_rx1_n_c1    : in std_logic;
    qsfp1_rx1_p_c1    : in std_logic;
                 
    qsfp2_tx1_n_c1    : out std_logic;
    qsfp2_tx1_p_c1    : out std_logic;
    qsfp2_rx1_n_c1    : in std_logic;
    qsfp2_rx1_p_c1    : in std_logic;

    qsfp1_tx1_n_c2    : out std_logic;
    qsfp1_tx1_p_c2    : out std_logic;
    qsfp1_rx1_n_c2    : in std_logic;
    qsfp1_rx1_p_c2    : in std_logic;
                 
    qsfp2_tx1_n_c2    : out std_logic;
    qsfp2_tx1_p_c2    : out std_logic;
    qsfp2_rx1_n_c2    : in std_logic;
    qsfp2_rx1_p_c2    : in std_logic;

    qsfp1_tx1_n_c3    : out std_logic;
    qsfp1_tx1_p_c3    : out std_logic;
    qsfp1_rx1_n_c3    : in std_logic;
    qsfp1_rx1_p_c3    : in std_logic;
                 
    qsfp2_tx1_n_c3    : out std_logic;
    qsfp2_tx1_p_c3    : out std_logic;
    qsfp2_rx1_n_c3    : in std_logic;
    qsfp2_rx1_p_c3    : in std_logic;

    usr_func_wr       : in std_logic_vector(16383 downto 0);
    usr_wen           : in std_logic;
    usr_data_wr       : in std_logic_vector(63 downto 0);
    usr_func_rd       : in std_logic_vector(16383 downto 0);
    usr_rden          : in std_logic;
    read_data         : out std_logic_vector(63 downto 0);

    lff_in            : in std_logic;

    out_clk           : out std_logic;
    link_down         : out std_logic;
    link_full         : out std_logic;

    Rst_hrd_sim       : in std_logic
    );
end Level_2_4c;


--------------------------------------------------------------------------------

architecture Behavioral of Level_2_4c is

-------------------------------------------- Components ---------------------
  
  component clk_wiz_0
    port (
      clk_out1          : out    std_logic;
      clk_out2          : out    std_logic;
      reset             : in     std_logic;
      clk_in1_p         : in     std_logic;
      clk_in1_n         : in     std_logic
      );
  end component;



  COMPONENT SR_sender_GLB_0
    PORT (
      aresetn              : IN STD_LOGIC;
      txdiffctrl_in        : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txpostcursor_in      : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txprecursor_in       : IN STD_LOGIC_VECTOR(4 DOWNTO 0);

      Core_status_addr     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Core_status_data_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      user_100MHz_clk      : IN STD_LOGIC;
      FED_CLOCK            : IN STD_LOGIC;
      event_data_word      : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
      event_ctrl           : IN STD_LOGIC;
      event_data_wen       : IN STD_LOGIC;
      backpressure         : OUT STD_LOGIC;
      Link_DOWN_n          : OUT STD_LOGIC;
      ext_trigger          : IN STD_LOGIC;
      ext_veto_out         : OUT STD_LOGIC;
      qpll_lock_in         : IN STD_LOGIC;
      qpll_reset_out       : OUT STD_LOGIC;
      qpll_clkin           : IN STD_LOGIC;
      qpll_ref_clkin       : IN STD_LOGIC;
      Snd_gtyrxn_in        : IN STD_LOGIC;
      Snd_gtyrxp_in        : IN STD_LOGIC;
      Snd_gtytxn_out       : OUT STD_LOGIC;
      Snd_gtytxp_out       : OUT STD_LOGIC;
      Rst_hrd_sim          : IN STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_sender_GLB_1
    PORT (
      aresetn              : IN STD_LOGIC;
      txdiffctrl_in        : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txpostcursor_in      : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txprecursor_in       : IN STD_LOGIC_VECTOR(4 DOWNTO 0);

      Core_status_addr     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Core_status_data_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      user_100MHz_clk      : IN STD_LOGIC;
      FED_CLOCK            : IN STD_LOGIC;
      event_data_word      : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
      event_ctrl           : IN STD_LOGIC;
      event_data_wen       : IN STD_LOGIC;
      backpressure         : OUT STD_LOGIC;
      Link_DOWN_n          : OUT STD_LOGIC;
      ext_trigger          : IN STD_LOGIC;
      ext_veto_out         : OUT STD_LOGIC;
      qpll_lock_in         : IN STD_LOGIC;
      qpll_reset_out       : OUT STD_LOGIC;
      qpll_clkin           : IN STD_LOGIC;
      qpll_ref_clkin       : IN STD_LOGIC;
      Snd_gtyrxn_in        : IN STD_LOGIC;
      Snd_gtyrxp_in        : IN STD_LOGIC;
      Snd_gtytxn_out       : OUT STD_LOGIC;
      Snd_gtytxp_out       : OUT STD_LOGIC;
      Rst_hrd_sim          : IN STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_sender_GLB_2
    PORT (
      aresetn              : IN STD_LOGIC;
      txdiffctrl_in        : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txpostcursor_in      : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txprecursor_in       : IN STD_LOGIC_VECTOR(4 DOWNTO 0);

      Core_status_addr     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Core_status_data_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      user_100MHz_clk      : IN STD_LOGIC;
      FED_CLOCK            : IN STD_LOGIC;
      event_data_word      : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
      event_ctrl           : IN STD_LOGIC;
      event_data_wen       : IN STD_LOGIC;
      backpressure         : OUT STD_LOGIC;
      Link_DOWN_n          : OUT STD_LOGIC;
      ext_trigger          : IN STD_LOGIC;
      ext_veto_out         : OUT STD_LOGIC;
      qpll_lock_in         : IN STD_LOGIC;
      qpll_reset_out       : OUT STD_LOGIC;
      qpll_clkin           : IN STD_LOGIC;
      qpll_ref_clkin       : IN STD_LOGIC;
      Snd_gtyrxn_in        : IN STD_LOGIC;
      Snd_gtyrxp_in        : IN STD_LOGIC;
      Snd_gtytxn_out       : OUT STD_LOGIC;
      Snd_gtytxp_out       : OUT STD_LOGIC;
      Rst_hrd_sim          : IN STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_sender_GLB_3
    PORT (
      aresetn              : IN STD_LOGIC;
      txdiffctrl_in        : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txpostcursor_in      : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      txprecursor_in       : IN STD_LOGIC_VECTOR(4 DOWNTO 0);

      Core_status_addr     : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Core_status_data_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      user_100MHz_clk      : IN STD_LOGIC;
      FED_CLOCK            : IN STD_LOGIC;
      event_data_word      : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
      event_ctrl           : IN STD_LOGIC;
      event_data_wen       : IN STD_LOGIC;
      backpressure         : OUT STD_LOGIC;
      Link_DOWN_n          : OUT STD_LOGIC;
      ext_trigger          : IN STD_LOGIC;
      ext_veto_out         : OUT STD_LOGIC;
      qpll_lock_in         : IN STD_LOGIC;
      qpll_reset_out       : OUT STD_LOGIC;
      qpll_clkin           : IN STD_LOGIC;
      qpll_ref_clkin       : IN STD_LOGIC;
      Snd_gtyrxn_in        : IN STD_LOGIC;
      Snd_gtyrxp_in        : IN STD_LOGIC;
      Snd_gtytxn_out       : OUT STD_LOGIC;
      Snd_gtytxp_out       : OUT STD_LOGIC;
      Rst_hrd_sim          : IN STD_LOGIC

      );
  END COMPONENT;

  component QPLL_wrapper_SR25_GTY_ref15625
    Port ( 
      gtrefclk00_in				: in std_logic;
      gtrefclk01_in       		: in std_logic;
      qpll0reset_in       		: in std_logic;
      qpll1reset_in       		: in std_logic;
      
      qpll0lock_in        		: out std_logic;
      qpll0outclk_out     		: out std_logic;
      qpll0outrefclk_out  		: out std_logic;
      
      qpll1lock_in        		: out std_logic;
      qpll1outclk_out     		: out std_logic;
      qpll1outrefclk_out  		: out std_logic
      );
  end component;


-------------- for testing with a receiver -------------------------------------
  component EventGenerator
    generic( EVENT_SIZE : integer );
    port(
      clk            : in std_logic;
      reset          : in std_logic;
      lff            : in std_logic;
      link_down_n    : in std_logic;
      event_data     : out std_logic_vector(127 downto 0);
      event_data_wen : out std_logic;
      event_ctrl     : out std_logic
      );
  end component;
  
  component MonitoringCounters
    generic(
      adr_offset : integer
      );
    port (
      fed_clk            : in std_logic;
      reset_counters     : in std_logic;
      ena_cnt_pckt       : in std_logic;
      ena_cnt_bad        : in std_logic;
      ena_cnt_backpr     : in std_logic;
      ena_cnt_fedcrc     : in std_logic;
      ena_cnt_slinkcrc   : in std_logic;
      usr_func_rd        : in std_logic_vector( 16383 downto 0 );
      counter_data       : out std_logic_vector( 63 downto 0 );
      readCounterActive  : out std_logic
      );
  end component;
  
  COMPONENT SR_Receive_GLB_0
    PORT (
      usr_clk           : IN STD_LOGIC;
      rst_usr_clk_n     : IN STD_LOGIC;
      usr_func_wr       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_wen           : IN STD_LOGIC;
      usr_data_wr       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);

      usr_func_rd       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_rden          : IN STD_LOGIC;
      usr_dto_receiver  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      
      user_100MHz_clk   : IN STD_LOGIC;
      
      clk_out           : OUT STD_LOGIC;
      wen_data          : OUT STD_LOGIC;
      UCTRL_out         : OUT STD_LOGIC;
      data_out          : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
      lff               : IN STD_LOGIC;

      ena_cnt_pckt      : OUT STD_LOGIC;
      ena_cnt_bad_p     : OUT STD_LOGIC;
      ena_back_pres     : OUT STD_LOGIC;
      ena_FED_crc_err   : OUT STD_LOGIC;
      ena_SLINK_crc_err : OUT STD_LOGIC;

      qpll_lock_in      : IN STD_LOGIC;
      qpll_reset_out    : OUT STD_LOGIC;
      qpll_clkin        : IN STD_LOGIC;
      qpll_ref_clkin    : IN STD_LOGIC;

      Rcv_gtyrxn_in     : IN STD_LOGIC;
      Rcv_gtyrxp_in     : IN STD_LOGIC;
      Rcv_gtytxn_out    : OUT STD_LOGIC;
      Rcv_gtytxp_out    : OUT STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_Receive_GLB_1
    PORT (
      usr_clk           : IN STD_LOGIC;
      rst_usr_clk_n     : IN STD_LOGIC;
      usr_func_wr       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_wen           : IN STD_LOGIC;
      usr_data_wr       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);

      usr_func_rd       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_rden          : IN STD_LOGIC;
      usr_dto_receiver  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      
      user_100MHz_clk   : IN STD_LOGIC;
      
      clk_out           : OUT STD_LOGIC;
      wen_data          : OUT STD_LOGIC;
      UCTRL_out         : OUT STD_LOGIC;
      data_out          : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
      lff               : IN STD_LOGIC;

      ena_cnt_pckt      : OUT STD_LOGIC;
      ena_cnt_bad_p     : OUT STD_LOGIC;
      ena_back_pres     : OUT STD_LOGIC;
      ena_FED_crc_err   : OUT STD_LOGIC;
      ena_SLINK_crc_err : OUT STD_LOGIC;

      qpll_lock_in      : IN STD_LOGIC;
      qpll_reset_out    : OUT STD_LOGIC;
      qpll_clkin        : IN STD_LOGIC;
      qpll_ref_clkin    : IN STD_LOGIC;

      Rcv_gtyrxn_in     : IN STD_LOGIC;
      Rcv_gtyrxp_in     : IN STD_LOGIC;
      Rcv_gtytxn_out    : OUT STD_LOGIC;
      Rcv_gtytxp_out    : OUT STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_Receive_GLB_2
    PORT (
      usr_clk           : IN STD_LOGIC;
      rst_usr_clk_n     : IN STD_LOGIC;
      usr_func_wr       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_wen           : IN STD_LOGIC;
      usr_data_wr       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);

      usr_func_rd       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_rden          : IN STD_LOGIC;
      usr_dto_receiver  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      
      user_100MHz_clk   : IN STD_LOGIC;
      
      clk_out           : OUT STD_LOGIC;
      wen_data          : OUT STD_LOGIC;
      UCTRL_out         : OUT STD_LOGIC;
      data_out          : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
      lff               : IN STD_LOGIC;

      ena_cnt_pckt      : OUT STD_LOGIC;
      ena_cnt_bad_p     : OUT STD_LOGIC;
      ena_back_pres     : OUT STD_LOGIC;
      ena_FED_crc_err   : OUT STD_LOGIC;
      ena_SLINK_crc_err : OUT STD_LOGIC;

      qpll_lock_in      : IN STD_LOGIC;
      qpll_reset_out    : OUT STD_LOGIC;
      qpll_clkin        : IN STD_LOGIC;
      qpll_ref_clkin    : IN STD_LOGIC;

      Rcv_gtyrxn_in     : IN STD_LOGIC;
      Rcv_gtyrxp_in     : IN STD_LOGIC;
      Rcv_gtytxn_out    : OUT STD_LOGIC;
      Rcv_gtytxp_out    : OUT STD_LOGIC

      );
  END COMPONENT;

  COMPONENT SR_Receive_GLB_3
    PORT (
      usr_clk           : IN STD_LOGIC;
      rst_usr_clk_n     : IN STD_LOGIC;
      usr_func_wr       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_wen           : IN STD_LOGIC;
      usr_data_wr       : IN STD_LOGIC_VECTOR(63 DOWNTO 0);

      usr_func_rd       : IN STD_LOGIC_VECTOR(16383 DOWNTO 0);
      usr_rden          : IN STD_LOGIC;
      usr_dto_receiver  : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      
      user_100MHz_clk   : IN STD_LOGIC;
      
      clk_out           : OUT STD_LOGIC;
      wen_data          : OUT STD_LOGIC;
      UCTRL_out         : OUT STD_LOGIC;
      data_out          : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
      lff               : IN STD_LOGIC;

      ena_cnt_pckt      : OUT STD_LOGIC;
      ena_cnt_bad_p     : OUT STD_LOGIC;
      ena_back_pres     : OUT STD_LOGIC;
      ena_FED_crc_err   : OUT STD_LOGIC;
      ena_SLINK_crc_err : OUT STD_LOGIC;

      qpll_lock_in      : IN STD_LOGIC;
      qpll_reset_out    : OUT STD_LOGIC;
      qpll_clkin        : IN STD_LOGIC;
      qpll_ref_clkin    : IN STD_LOGIC;

      Rcv_gtyrxn_in     : IN STD_LOGIC;
      Rcv_gtyrxp_in     : IN STD_LOGIC;
      Rcv_gtytxn_out    : OUT STD_LOGIC;
      Rcv_gtytxp_out    : OUT STD_LOGIC

      );
  END COMPONENT;


-------------------------------- Signals -----------------------------------------

-- basic
  signal clk              : std_logic;
  signal reset            : std_logic;
  signal reset_n          : std_logic;
  signal resetCounter     : std_logic := '0';

-- Slink interface
  signal event_data_c0    : std_logic_vector( 127 downto 0);
  signal event_ctrl_c0    : std_logic;
  signal event_data_wen_c0: std_logic;
  signal lff_c0           : std_logic;
  signal link_down_n_c0   : std_logic;

  signal event_data_c1    : std_logic_vector( 127 downto 0);
  signal event_ctrl_c1    : std_logic;
  signal event_data_wen_c1: std_logic;
  signal lff_c1           : std_logic;
  signal link_down_n_c1   : std_logic;

  signal event_data_c2    : std_logic_vector( 127 downto 0);
  signal event_ctrl_c2    : std_logic;
  signal event_data_wen_c2: std_logic;
  signal lff_c2           : std_logic;
  signal link_down_n_c2   : std_logic;

  signal event_data_c3    : std_logic_vector( 127 downto 0);
  signal event_ctrl_c3    : std_logic;
  signal event_data_wen_c3: std_logic;
  signal lff_c3           : std_logic;
  signal link_down_n_c3   : std_logic;

-- connects the QPLL wrapper clock signals to the sender
  signal qpll_reset_sender_c0   : std_logic;
  signal qpll_reset_sender_c1   : std_logic;
  signal qpll_reset_sender_c2   : std_logic;
  signal qpll_reset_sender_c3   : std_logic;
  signal qpll_clk               : std_logic;
  signal qpll_ref_clk           : std_logic;
  signal qpll_lock              : std_logic;

-- connects the QPLL wrapper clock signals to the receiver
  signal qpll_reset_receiver_c0 : std_logic;
  signal qpll_reset_receiver_c1 : std_logic;
  signal qpll_reset_receiver_c2 : std_logic;
  signal qpll_reset_receiver_c3 : std_logic;
  signal qpll_clk_r             : std_logic;
  signal qpll_ref_clk_r         : std_logic;
  signal qpll_lock_r            : std_logic;

-- output of clock buffer for transceiver
  signal gt_ref_clk     : std_logic;
  signal qpll_reset     : std_logic;

-- for the receiver part
  signal wen_data_c0             : std_logic;
  signal receiver_data_c0        : std_logic_vector( 63 downto 0 );
  signal fed_clk_out_c0          : std_logic;
  signal UCTRL_out_c0            : std_logic;
  signal data_out_c0             : std_logic_vector( 127 downto 0 );
  signal ena_cnt_pckt_c0         : std_logic;
  signal ena_cnt_bad_c0          : std_logic;
  signal ena_cnt_backpr_c0       : std_logic;
  signal ena_cnt_fedcrc_c0       : std_logic;
  signal ena_cnt_slinkcrc_c0     : std_logic;

  signal wen_data_c1             : std_logic;
  signal receiver_data_c1        : std_logic_vector( 63 downto 0 );
  signal fed_clk_out_c1          : std_logic;
  signal UCTRL_out_c1            : std_logic;
  signal data_out_c1             : std_logic_vector( 127 downto 0 );
  signal ena_cnt_pckt_c1         : std_logic;
  signal ena_cnt_bad_c1          : std_logic;
  signal ena_cnt_backpr_c1       : std_logic;
  signal ena_cnt_fedcrc_c1       : std_logic;
  signal ena_cnt_slinkcrc_c1     : std_logic;

  signal wen_data_c2             : std_logic;
  signal receiver_data_c2        : std_logic_vector( 63 downto 0 );
  signal fed_clk_out_c2          : std_logic;
  signal UCTRL_out_c2            : std_logic;
  signal data_out_c2             : std_logic_vector( 127 downto 0 );
  signal ena_cnt_pckt_c2         : std_logic;
  signal ena_cnt_bad_c2          : std_logic;
  signal ena_cnt_backpr_c2       : std_logic;
  signal ena_cnt_fedcrc_c2       : std_logic;
  signal ena_cnt_slinkcrc_c2     : std_logic;

  signal wen_data_c3             : std_logic;
  signal receiver_data_c3        : std_logic_vector( 63 downto 0 );
  signal fed_clk_out_c3          : std_logic;
  signal UCTRL_out_c3            : std_logic;
  signal data_out_c3             : std_logic_vector( 127 downto 0 );
  signal ena_cnt_pckt_c3         : std_logic;
  signal ena_cnt_bad_c3          : std_logic;
  signal ena_cnt_backpr_c3       : std_logic;
  signal ena_cnt_fedcrc_c3       : std_logic;
  signal ena_cnt_slinkcrc_c3     : std_logic;


  signal core_status_data_out_c0 : std_logic_vector( 63 downto 0 );
  signal core_status_data_out_c1 : std_logic_vector( 63 downto 0 );
  signal core_status_data_out_c2 : std_logic_vector( 63 downto 0 );
  signal core_status_data_out_c3 : std_logic_vector( 63 downto 0 );
  signal status_adr              : std_logic_vector( 15 downto 0 );

  signal counter_data_c0         : std_logic_vector( 63 downto 0 );
  signal counter_data_c1         : std_logic_vector( 63 downto 0 );
  signal counter_data_c2         : std_logic_vector( 63 downto 0 );
  signal counter_data_c3         : std_logic_vector( 63 downto 0 );
  signal readCounterActive_c0    : std_logic;
  signal readCounterActive_c1    : std_logic;
  signal readCounterActive_c2    : std_logic;
  signal readCounterActive_c3    : std_logic;

  signal receiver_read_c0        : std_logic;
  signal receiver_read_c1        : std_logic;
  signal receiver_read_c2        : std_logic;
  signal receiver_read_c3        : std_logic;
begin

------------ make clock available outside (for simulation) --------------

  out_clk <= clk;

--------------------- Reset signals from GPIO ---------------------------
  reset <= GPIO_SW_N;
  reset_n <= not( GPIO_SW_N );

---------------- Preparation of Slink Flow Control signals --------------
  link_down <= not (link_down_n_c0 and link_down_n_c1 and link_down_n_c2 and link_down_n_c3);
  link_full <= lff_c0 or lff_c1 or lff_c2 or lff_c3;

------------------------ QPLL reset logic -------------------------------

  qpll_reset <= '1' WHEN qpll_reset_sender_c0 = '1' or qpll_reset_sender_c1 = '1' or
                qpll_reset_sender_c2 = '1'  or qpll_reset_sender_c3 = '1' or
                qpll_reset_receiver_c0 = '1' or qpll_reset_receiver_c1 = '1' or
                qpll_reset_receiver_c2 = '1' or qpll_reset_receiver_c3 = '1'
                else '0'; 

-------------------------------------------------------------------------

  clockdriver : clk_wiz_0
    port map ( 
      -- Clock in ports
      clk_in1_p => u250mhz_clk1_p,
      clk_in1_n => u250mhz_clk1_n,
      -- Clock out ports  
      clk_out1 => clk,         -- 100MHz
      -- clk_out2 => clk_out2,    -- 150MHz not used
      reset     => '0'
      );

  EventGenerator_c0: EventGenerator
    generic map (
      EVENT_SIZE     => 8
      )
    port map (
      clk            => clk,
      reset          => reset,
      lff            => lff_c0,
      link_down_n    => link_down_n_c0,
      event_data     => event_data_c0,
      event_data_wen => event_data_wen_c0,
      event_ctrl     => event_ctrl_c0
      ); 

  EventGenerator_c1: EventGenerator
    generic map (
      EVENT_SIZE     => 8
      )
    port map (
      clk            => clk,
      reset          => reset,
      lff            => lff_c1,
      link_down_n    => link_down_n_c1,
      event_data     => event_data_c1,
      event_data_wen => event_data_wen_c1,
      event_ctrl     => event_ctrl_c1
      ); 

  EventGenerator_c2: EventGenerator
    generic map (
      EVENT_SIZE     => 8
      )
    port map (
      clk            => clk,
      reset          => reset,
      lff            => lff_c2,
      link_down_n    => link_down_n_c2,
      event_data     => event_data_c2,
      event_data_wen => event_data_wen_c2,
      event_ctrl     => event_ctrl_c2
      ); 

  EventGenerator_c3: EventGenerator
    generic map (
      EVENT_SIZE     => 8
      )
    port map (
      clk            => clk,
      reset          => reset,
      lff            => lff_c3,
      link_down_n    => link_down_n_c3,
      event_data     => event_data_c3,
      event_data_wen => event_data_wen_c3,
      event_ctrl     => event_ctrl_c3
      ); 

  slinkSender_c0 : SR_sender_GLB_0
    PORT MAP (
      aresetn           => reset_n,
      txdiffctrl_in     => "11000",
      txpostcursor_in   => "10100",
      txprecursor_in    => "00000",
      
      Core_status_addr     => status_adr,
      Core_status_data_out => core_status_data_out_c0,

      user_100MHz_clk => clk,
      FED_CLOCK       => clk,
      event_data_word => event_data_c0,
      event_ctrl      => event_ctrl_c0,
      event_data_wen  => event_data_wen_c0,
      backpressure    => lff_c0,
      Link_DOWN_n     => link_down_n_c0, -- active low!
      ext_trigger     => '0',
      --ext_veto_out => ext_veto_out,

      qpll_lock_in    => qpll_lock,
      qpll_reset_out  => qpll_reset_sender_c0,
      qpll_clkin      => qpll_clk,
      qpll_ref_clkin  => qpll_ref_clk,
      
      Snd_gtyrxn_in   => qsfp1_rx1_n_c0,
      Snd_gtyrxp_in   => qsfp1_rx1_p_c0,
      Snd_gtytxn_out  => qsfp1_tx1_n_c0,
      Snd_gtytxp_out  => qsfp1_tx1_p_c0,
      Rst_hrd_sim     => Rst_hrd_sim

      );

  slinkSender_c1 : SR_sender_GLB_0
    PORT MAP (
      aresetn           => reset_n,
      txdiffctrl_in     => "11000",
      txpostcursor_in   => "10100",
      txprecursor_in    => "00000",
      
      Core_status_addr     => status_adr,
      Core_status_data_out => core_status_data_out_c1,

      user_100MHz_clk => clk,
      FED_CLOCK       => clk,
      event_data_word => event_data_c1,
      event_ctrl      => event_ctrl_c1,
      event_data_wen  => event_data_wen_c1,
      backpressure    => lff_c1,
      Link_DOWN_n     => link_down_n_c1, -- active low!
      ext_trigger     => '0',
      --ext_veto_out => ext_veto_out,

      qpll_lock_in    => qpll_lock,
      qpll_reset_out  => qpll_reset_sender_c1,
      qpll_clkin      => qpll_clk,
      qpll_ref_clkin  => qpll_ref_clk,
      
      Snd_gtyrxn_in   => qsfp1_rx1_n_c1,
      Snd_gtyrxp_in   => qsfp1_rx1_p_c1,
      Snd_gtytxn_out  => qsfp1_tx1_n_c1,
      Snd_gtytxp_out  => qsfp1_tx1_p_c1,
      Rst_hrd_sim     => Rst_hrd_sim

      );

  slinkSender_c2 : SR_sender_GLB_0
    PORT MAP (
      aresetn           => reset_n,
      txdiffctrl_in     => "11000",
      txpostcursor_in   => "10100",
      txprecursor_in    => "00000",
      
      Core_status_addr     => status_adr,
      Core_status_data_out => core_status_data_out_c2,

      user_100MHz_clk => clk,
      FED_CLOCK       => clk,
      event_data_word => event_data_c2,
      event_ctrl      => event_ctrl_c2,
      event_data_wen  => event_data_wen_c2,
      backpressure    => lff_c2,
      Link_DOWN_n     => link_down_n_c2, -- active low!
      ext_trigger     => '0',
      --ext_veto_out => ext_veto_out,

      qpll_lock_in    => qpll_lock,
      qpll_reset_out  => qpll_reset_sender_c2,
      qpll_clkin      => qpll_clk,
      qpll_ref_clkin  => qpll_ref_clk,
      
      Snd_gtyrxn_in   => qsfp1_rx1_n_c2,
      Snd_gtyrxp_in   => qsfp1_rx1_p_c2,
      Snd_gtytxn_out  => qsfp1_tx1_n_c2,
      Snd_gtytxp_out  => qsfp1_tx1_p_c2,
      Rst_hrd_sim     => Rst_hrd_sim

      );

  slinkSender_c3 : SR_sender_GLB_0
    PORT MAP (
      aresetn           => reset_n,
      txdiffctrl_in     => "11000",
      txpostcursor_in   => "10100",
      txprecursor_in    => "00000",
      
      Core_status_addr     => status_adr,
      Core_status_data_out => core_status_data_out_c3,

      user_100MHz_clk => clk,
      FED_CLOCK       => clk,
      event_data_word => event_data_c3,
      event_ctrl      => event_ctrl_c3,
      event_data_wen  => event_data_wen_c3,
      backpressure    => lff_c3,
      Link_DOWN_n     => link_down_n_c3, -- active low!
      ext_trigger     => '0',
      --ext_veto_out => ext_veto_out,

      qpll_lock_in    => qpll_lock,
      qpll_reset_out  => qpll_reset_sender_c3,
      qpll_clkin      => qpll_clk,
      qpll_ref_clkin  => qpll_ref_clk,
      
      Snd_gtyrxn_in   => qsfp1_rx1_n_c3,
      Snd_gtyrxp_in   => qsfp1_rx1_p_c3,
      Snd_gtytxn_out  => qsfp1_tx1_n_c3,
      Snd_gtytxp_out  => qsfp1_tx1_p_c3,
      Rst_hrd_sim     => Rst_hrd_sim

      );

--**************************     Ref clock buffer      **************************************  

  IBUFDS_GTE4_inst : IBUFDS_GTE4
    port map (
      O     	=> gt_ref_clk, -- 1-bit output: Refer to Transceiver User Guide
      CEB   	=> '0', -- 1-bit input: Refer to Transceiver User Guide 
      I     	=> qpll_ref_clock_p, -- 1-bit input: Refer to Transceiver User Guide 
      IB        => qpll_ref_clock_n -- 1-bit input: Refer to Transceiver User Guide
      );  


  qpll_I1:QPLL_wrapper_SR25_GTY_ref15625
    Port MAP( 
      gtrefclk00_in		        => gt_ref_clk,
      gtrefclk01_in       		=> '0',
      qpll0reset_in       		=> qpll_reset,
      qpll1reset_in       		=> reset,
      
      qpll0lock_in        		=> qpll_lock,
      qpll0outclk_out     		=> qpll_clk,
      qpll0outrefclk_out  		=> qpll_ref_clk
      );

  qpll_I1_r:QPLL_wrapper_SR25_GTY_ref15625
    Port MAP( 
      gtrefclk00_in		        => gt_ref_clk,
      gtrefclk01_in       		=> '0',
      qpll0reset_in       		=> qpll_reset,
      qpll1reset_in       		=> reset,
      
      qpll0lock_in        		=> qpll_lock_r,
      qpll0outclk_out     		=> qpll_clk_r,
      qpll0outrefclk_out  		=> qpll_ref_clk_r
      
      );

------------------------- Receiver part ----------------------------

  
  slink_receiver_c0 : SR_Receive_GLB_0
    PORT MAP (
      usr_clk           => pcie_clk,
      rst_usr_clk_n     => reset_n,
      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,
      usr_data_wr       => usr_data_wr,

      usr_func_rd       => usr_func_rd,
      usr_rden          => usr_rden,
      usr_dto_receiver  => receiver_data_c0,

      user_100MHz_clk   => clk,

      clk_out           => fed_clk_out_c0,
      wen_data          => wen_data_c0,
      UCTRL_out         => UCTRL_out_c0,
      data_out          => data_out_c0,
      lff               => lff_in,

      ena_cnt_pckt      => ena_cnt_pckt_c0,
      ena_cnt_bad_p     => ena_cnt_bad_c0,
      ena_back_pres     => ena_cnt_backpr_c0,
      ena_FED_crc_err   => ena_cnt_fedcrc_c0,
      ena_SLINK_crc_err => ena_cnt_slinkcrc_c0,

      qpll_lock_in      => qpll_lock_r,
      qpll_reset_out    => qpll_reset_receiver_c0,
      qpll_clkin        => qpll_clk_r,
      qpll_ref_clkin    => qpll_ref_clk_r,

      Rcv_gtyrxn_in     => qsfp2_rx1_n_c0,
      Rcv_gtyrxp_in     => qsfp2_rx1_p_c0,
      Rcv_gtytxn_out    => qsfp2_tx1_n_c0,
      Rcv_gtytxp_out    => qsfp2_tx1_p_c0
      );

  slink_receiver_c1 : SR_Receive_GLB_1
    PORT MAP (
      usr_clk           => pcie_clk,
      rst_usr_clk_n     => reset_n,
      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,
      usr_data_wr       => usr_data_wr,

      usr_func_rd       => usr_func_rd,
      usr_rden          => usr_rden,
      usr_dto_receiver  => receiver_data_c1,

      user_100MHz_clk   => clk,

      clk_out           => fed_clk_out_c1,
      wen_data          => wen_data_c1,
      UCTRL_out         => UCTRL_out_c1,
      data_out          => data_out_c1,
      lff               => lff_in,

      ena_cnt_pckt      => ena_cnt_pckt_c1,
      ena_cnt_bad_p     => ena_cnt_bad_c1,
      ena_back_pres     => ena_cnt_backpr_c1,
      ena_FED_crc_err   => ena_cnt_fedcrc_c1,
      ena_SLINK_crc_err => ena_cnt_slinkcrc_c1,

      qpll_lock_in      => qpll_lock_r,
      qpll_reset_out    => qpll_reset_receiver_c1,
      qpll_clkin        => qpll_clk_r,
      qpll_ref_clkin    => qpll_ref_clk_r,

      Rcv_gtyrxn_in     => qsfp2_rx1_n_c1,
      Rcv_gtyrxp_in     => qsfp2_rx1_p_c1,
      Rcv_gtytxn_out    => qsfp2_tx1_n_c1,
      Rcv_gtytxp_out    => qsfp2_tx1_p_c1
      );

  slink_receiver_c2 : SR_Receive_GLB_2
    PORT MAP (
      usr_clk           => pcie_clk,
      rst_usr_clk_n     => reset_n,
      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,
      usr_data_wr       => usr_data_wr,

      usr_func_rd       => usr_func_rd,
      usr_rden          => usr_rden,
      usr_dto_receiver  => receiver_data_c2,

      user_100MHz_clk   => clk,

      clk_out           => fed_clk_out_c2,
      wen_data          => wen_data_c2,
      UCTRL_out         => UCTRL_out_c2,
      data_out          => data_out_c2,
      lff               => lff_in,

      ena_cnt_pckt      => ena_cnt_pckt_c2,
      ena_cnt_bad_p     => ena_cnt_bad_c2,
      ena_back_pres     => ena_cnt_backpr_c2,
      ena_FED_crc_err   => ena_cnt_fedcrc_c2,
      ena_SLINK_crc_err => ena_cnt_slinkcrc_c2,

      qpll_lock_in      => qpll_lock_r,
      qpll_reset_out    => qpll_reset_receiver_c2,
      qpll_clkin        => qpll_clk_r,
      qpll_ref_clkin    => qpll_ref_clk_r,

      Rcv_gtyrxn_in     => qsfp2_rx1_n_c2,
      Rcv_gtyrxp_in     => qsfp2_rx1_p_c2,
      Rcv_gtytxn_out    => qsfp2_tx1_n_c2,
      Rcv_gtytxp_out    => qsfp2_tx1_p_c2
      );

  slink_receiver_c3 : SR_Receive_GLB_3
    PORT MAP (
      usr_clk           => pcie_clk,
      rst_usr_clk_n     => reset_n,
      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,
      usr_data_wr       => usr_data_wr,

      usr_func_rd       => usr_func_rd,
      usr_rden          => usr_rden,
      usr_dto_receiver  => receiver_data_c3,

      user_100MHz_clk   => clk,

      clk_out           => fed_clk_out_c3,
      wen_data          => wen_data_c3,
      UCTRL_out         => UCTRL_out_c3,
      data_out          => data_out_c3,
      lff               => lff_in,

      ena_cnt_pckt      => ena_cnt_pckt_c3,
      ena_cnt_bad_p     => ena_cnt_bad_c3,
      ena_back_pres     => ena_cnt_backpr_c3,
      ena_FED_crc_err   => ena_cnt_fedcrc_c3,
      ena_SLINK_crc_err => ena_cnt_slinkcrc_c3,

      qpll_lock_in      => qpll_lock_r,
      qpll_reset_out    => qpll_reset_receiver_c3,
      qpll_clkin        => qpll_clk_r,
      qpll_ref_clkin    => qpll_ref_clk_r,

      Rcv_gtyrxn_in     => qsfp2_rx1_n_c3,
      Rcv_gtyrxp_in     => qsfp2_rx1_p_c3,
      Rcv_gtytxn_out    => qsfp2_tx1_n_c3,
      Rcv_gtytxp_out    => qsfp2_tx1_p_c3
      );

  -- connect various counters which can be read out

  -- generate the resetCounter signal
  resetCntProc : process( clk, usr_wen )

  begin  
    if rising_edge( clk ) then
      if ( usr_func_wr( adr_reset_cnt ) = '1' and usr_wen = '1' ) then
        resetCounter <= '1';
      else
        resetCounter <= '0';
      end if;
    end if;  

  end process;
  
  monCounters_c0 : MonitoringCounters
    generic map(
      adr_offset => 0
      )
    port map (
      fed_clk           => fed_clk_out_c0,
      reset_counters    => resetCounter,
      ena_cnt_pckt      => ena_cnt_pckt_c0,
      ena_cnt_bad       => ena_cnt_bad_c0,
      ena_cnt_backpr    => ena_cnt_backpr_c0,
      ena_cnt_fedcrc    => ena_cnt_fedcrc_c0,
      ena_cnt_slinkcrc  => ena_cnt_slinkcrc_c0,
      usr_func_rd       => usr_func_rd,
      counter_data      => counter_data_c0,
      readCounterActive => readCounterActive_c0
      );

  monCounters_c1 : MonitoringCounters
    generic map (
      adr_offset => 16
      )
    port map (
      fed_clk           => fed_clk_out_c1,
      reset_counters    => resetCounter,
      ena_cnt_pckt      => ena_cnt_pckt_c1,
      ena_cnt_bad       => ena_cnt_bad_c1,
      ena_cnt_backpr    => ena_cnt_backpr_c1,
      ena_cnt_fedcrc    => ena_cnt_fedcrc_c1,
      ena_cnt_slinkcrc  => ena_cnt_slinkcrc_c1,
      usr_func_rd       => usr_func_rd,
      counter_data      => counter_data_c1,
      readCounterActive => readCounterActive_c1
      );

  monCounters_c2 : MonitoringCounters
    generic map (
      adr_offset => 32
      )
    port map (
      fed_clk           => fed_clk_out_c2,
      reset_counters    => resetCounter,
      ena_cnt_pckt      => ena_cnt_pckt_c2,
      ena_cnt_bad       => ena_cnt_bad_c2,
      ena_cnt_backpr    => ena_cnt_backpr_c2,
      ena_cnt_fedcrc    => ena_cnt_fedcrc_c2,
      ena_cnt_slinkcrc  => ena_cnt_slinkcrc_c2,
      usr_func_rd       => usr_func_rd,
      counter_data      => counter_data_c2,
      readCounterActive => readCounterActive_c2
      );

  monCounters_c3 : MonitoringCounters
    generic map (
      adr_offset => 48
      )
    port map (
      fed_clk           => fed_clk_out_c3,
      reset_counters    => resetCounter,
      ena_cnt_pckt      => ena_cnt_pckt_c3,
      ena_cnt_bad       => ena_cnt_bad_c3,
      ena_cnt_backpr    => ena_cnt_backpr_c3,
      ena_cnt_fedcrc    => ena_cnt_fedcrc_c3,
      ena_cnt_slinkcrc  => ena_cnt_slinkcrc_c3,
      usr_func_rd       => usr_func_rd,
      counter_data      => counter_data_c3,
      readCounterActive => readCounterActive_c3
      );

  -------------------- register for addressig the status words of the Slink Sender -----------------------
  --        The status information appears at the output of the slink sender
  --        core when the address is set. The reading of the status work is
  --        included in the read process above. 

  statusAdrReg : process( clk, usr_func_wr, usr_wen )
  begin
    if ( rising_edge(clk) and usr_wen='1' and  ( usr_func_wr( adr_sender_status_c0 ) = '1'  or
                                                 usr_func_wr( adr_sender_status_c1 ) = '1'  or
                                                 usr_func_wr( adr_sender_status_c2 ) = '1'  or
                                                 usr_func_wr( adr_sender_status_c3 ) = '1' ) ) then
      status_adr <= usr_data_wr( 15 downto 0 );
    end if;
  end process;

  receiver_read_c0 <= usr_func_rd(256) or usr_func_rd(257) or usr_func_rd(258) or usr_func_rd(259) or
                      usr_func_rd(260) or usr_func_rd(261) or usr_func_rd(262) or usr_func_rd(263); 
  receiver_read_c1 <= usr_func_rd(272) or usr_func_rd(273) or usr_func_rd(274) or usr_func_rd(275) or
                      usr_func_rd(276) or usr_func_rd(277) or usr_func_rd(278) or usr_func_rd(279); 
  receiver_read_c2 <= usr_func_rd(288) or usr_func_rd(289) or usr_func_rd(290) or usr_func_rd(291) or
                      usr_func_rd(292) or usr_func_rd(293) or usr_func_rd(294) or usr_func_rd(295); 
  receiver_read_c3 <= usr_func_rd(304) or usr_func_rd(305) or usr_func_rd(306) or usr_func_rd(307) or
                      usr_func_rd(308) or usr_func_rd(309) or usr_func_rd(310) or usr_func_rd(311); 
  
  rd_mplx : process( usr_func_rd, counter_data_c0, counter_data_c1, counter_data_c2, counter_data_c3,
                     receiver_data_c0, receiver_data_c1, receiver_data_c2, receiver_data_c3, 
                     core_status_data_out_c0, core_status_data_out_c1, core_status_data_out_c2, core_status_data_out_c3  )
  begin
  
    if usr_func_rd( adr_sender_status_c0 ) = '1' then
      read_data <= core_status_data_out_c0;
    elsif usr_func_rd( adr_sender_status_c1 ) = '1' then
      read_data <= core_status_data_out_c1;
    elsif usr_func_rd( adr_sender_status_c2 ) = '1' then
      read_data <= core_status_data_out_c2;
    elsif usr_func_rd( adr_sender_status_c3 ) = '1' then
      read_data <= core_status_data_out_c3;
    elsif readCounterActive_c0 = '1' then
      read_data <= counter_data_c0;
    elsif readCounterActive_c1 = '1' then
      read_data <= counter_data_c1;
    elsif readCounterActive_c2 = '1' then
      read_data <= counter_data_c2;
    elsif readCounterActive_c3 = '1' then
      read_data <= counter_data_c3;
    elsif receiver_read_c0 = '1' then      
      read_data <= receiver_data_c0;
    elsif receiver_read_c1 = '1' then      
      read_data <= receiver_data_c1;
    elsif receiver_read_c2 = '1' then      
      read_data <= receiver_data_c2;
    elsif receiver_read_c3 = '1' then      
      read_data <= receiver_data_c3;
      
    end if;
  end process;


end Behavioral;
