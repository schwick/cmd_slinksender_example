----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/05/2019 12:26:00 PM
-- Design Name: 
-- Module Name: top_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;
--use IEEE.std_logic_unsigned.all; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.top_address_constants.all;
use work.receiver_address_constants.all;
use work.PCIeSimulation.all;

entity top_sim is
    Port (
      usr_data_rd         : out std_logic_vector( 63 downto 0 )
--        GPIO_SW_N         : in std_logic;
--        u250mhz_clk1_n    : in std_logic;
--        u250mhz_clk1_p    : in std_logic;
--        qpll_ref_clock_n  : in std_logic;
--        qpll_ref_clock_p  : in std_logic;
--        qsfp1_tx1_n       : out std_logic;
--        qsfp1_tx1_p       : out std_logic;
--        qsfp1_rx1_n       : in std_logic;
--        qsfp1_rx1_p       : in std_logic;
--        
--        qsfp2_tx1_n       : out std_logic;
--        qsfp2_tx1_p       : out std_logic;
--        qsfp2_rx1_n       : in std_logic;
--        qsfp2_rx1_p       : in std_logic
        
        );
end top_sim;


--------------------------------------------------------------------------------

architecture Behavioral of top_sim is

  -- This address offset has to be set to the same offset which is defined
  -- in the instantanisation of the SLINK Receiver. By default it is 0. But
  -- if you change the generic offset parameter for the SLINK Receiver IP
  -- do not forget to adjust this constant accordingly. Othewise the simulation
  -- will not work since the PCI DAQ_ON command will not be decoded correctly.
  -- (Unfortunately this parameter cannot be passed down the hierarchy to the
  -- SLINK Receiver since the entity declaration of the Receiver, which is
  -- generated automatically from the Vivado gui, does not expose the generic
  -- of the IP. The philosophy of XLINX seems to be that you should define the
  -- parameters with the gui and then you are not allowed to mess around with
  -- them anymore.   
  
  constant address_offset : integer := 16;
  
-------------------------------------------- Components ---------------------


  component Level_2
    port (
      GPIO_SW_N         : in std_logic;
      u250mhz_clk1_n    : in std_logic;
      u250mhz_clk1_p    : in std_logic;
      pcie_clk          : in std_logic;
      qpll_ref_clock_n  : in std_logic;
      qpll_ref_clock_p  : in std_logic;
      qsfp1_tx1_n       : out std_logic;
      qsfp1_tx1_p       : out std_logic;
      qsfp1_rx1_n       : in std_logic;
      qsfp1_rx1_p       : in std_logic;
      
      qsfp2_tx1_n       : out std_logic;
      qsfp2_tx1_p       : out std_logic;
      qsfp2_rx1_n       : in std_logic;
      qsfp2_rx1_p       : in std_logic;
      
      usr_func_wr       : in std_logic_vector(16383 downto 0);
      usr_wen           : in std_logic;
      usr_data_wr       : in std_logic_vector(63 downto 0);
      usr_func_rd       : in std_logic_vector(16383 downto 0);
      usr_rden          : in std_logic;
      read_data         : out std_logic_vector(63 downto 0);
      
      lff_in            : in std_logic;

      out_clk           : out std_logic;

      Rst_hrd_sim       : in std_logic
      );
  end component;
  



-------------------------------- Signals -----------------------------------------

  signal usr_func_wr      : std_logic_vector( 16383 downto 0 ) := (others => '0');
  signal usr_cs_wr        : std_logic := '0';
  signal usr_wen          : std_logic := '0';
  signal usr_data_wr      : std_logic_vector( 63 downto 0 )   := (others => '0');
  signal usr_func_rd      : std_logic_vector( 16383 downto 0 ) := (others => '0');
  signal usr_cs_rd        : std_logic := '0';
  signal usr_rden         : std_logic := '0';
  signal lff_in           : std_logic := '0';

  signal level2_data      : std_logic_vector( 63 downto 0 );

  -- for simualtion
  signal u250mhz_clk1_n    : std_logic := '0';
  signal u250mhz_clk1_p    : std_logic := '1';

  signal pcie_clk           : std_logic := '0';
  -- coming from the clock wizard in Level2:
  signal clk               : std_logic;
  
  -- qsfp reference clock as the default ref clock on evaluation board:
  -- 156.25MHz 6.4ns period
  signal qpll_ref_clock_n  : std_logic := '0';
  signal qpll_ref_clock_p  : std_logic := '1';

  signal qsfp_stor_n       : std_logic;
  signal qsfp_stor_p       : std_logic;
  signal qsfp_rtos_n       : std_logic;
  signal qsfp_rtos_p       : std_logic;
  signal GPIO_SW_N         : std_logic;

  signal led_1_reg         : std_logic := '0';
  signal led_2_reg         : std_logic := '0';

  signal rst_hrd_sim       : std_logic := '0';
  
begin

  slink_interface : Level_2
    port map (
      GPIO_SW_N         => GPIO_SW_N,
      u250mhz_clk1_n    => u250mhz_clk1_n,   
      u250mhz_clk1_p    => u250mhz_clk1_p,
      pcie_clk          => pcie_clk,
      qpll_ref_clock_n  => qpll_ref_clock_n, 
      qpll_ref_clock_p  => qpll_ref_clock_p,

      -- sender:
      qsfp1_tx1_n       => qsfp_stor_n,
      qsfp1_tx1_p       => qsfp_stor_p,     
      qsfp1_rx1_n       => qsfp_rtos_n,     
      qsfp1_rx1_p       => qsfp_rtos_p,     
      --receiver:
      qsfp2_tx1_n       => qsfp_rtos_n,
      qsfp2_tx1_p       => qsfp_rtos_p,     
      qsfp2_rx1_n       => qsfp_stor_n,     
      qsfp2_rx1_p       => qsfp_stor_p,     

      usr_func_wr       => usr_func_wr,
      usr_wen           => usr_wen,      
      usr_data_wr       => usr_data_wr,     
      usr_func_rd       => usr_func_rd,     
      usr_rden          => usr_rden,      
      read_data         => level2_data,
      
      lff_in            => lff_in,           

      out_clk           => clk,

      Rst_hrd_sim       => rst_hrd_sim
      );


  lff_in <= '0';


  leds: process( pcie_clk, usr_func_wr, usr_wen )
  begin
    if rising_edge( pcie_clk ) then
      if ( usr_wen = '1' and usr_func_wr( adr_LED_test ) = '1' ) then
        led_1_reg <= usr_data_wr(0);
        led_2_reg <= usr_data_wr(1);
      end if;
    end if;
  end process;

  --GPIO_LED0 <= led_1_reg;
  --GPIO_LED1 <= led_2_reg;

  -- reading of the leds
  ledread: process( pcie_clk, usr_func_rd, usr_rden )
  begin
    if rising_edge( pcie_clk ) then
      if( usr_rden = '1' and usr_func_rd( adr_LED_test ) = '1' ) then
        usr_data_rd(0) <= led_1_reg;
        usr_data_rd(1) <= led_2_reg;
        usr_data_rd(63 downto 2) <= (others => '0');
      else
        usr_data_rd <= level2_data;
      end if;
    end if;
  end process;

  
  -- simulation specific
  -- make a 100MHz user clock (normally coming from PCIe interface)
  pcie_clk <= not pcie_clk after 10ns;
  
  -- make a 250MHz clock (normally coming from board)
  u250mhz_clk1_p <= not u250mhz_clk1_p after 2ns;
  u250mhz_clk1_n <= not u250mhz_clk1_n after 2ns;

  -- make a 156.250MHz ref clock for qpll (normally coming from board)
  qpll_ref_clock_n <= not qpll_ref_clock_n after 3.2ns;
  qpll_ref_clock_p <= not qpll_ref_clock_p after 3.2ns;

  -- power on reset
  GPIO_SW_N <= '1','0' after 5us;
    
  -- Reset the sender core
  process
  begin
    rst_hrd_sim <= '0';
    wait for 2 us;
    wait until falling_edge(clk) ;
    rst_hrd_sim <= '1';
    wait for 150 ns;
    wait until falling_edge(clk) ;
    rst_hrd_sim <= '0';
    wait;

  end process;  


  process
  begin
    usr_func_wr <= (others => '0');
    usr_func_rd <= (others => '0');
    usr_data_wr <= (others => '0');

    usr_wen <= '0';
    usr_rden <= '0';
    usr_cs_wr <= '0';
    usr_cs_rd <= '0';
    
--    wait for 50 us;
--    PCIe_func( clk, Slinkrocket_access_data, '0', x"0000000040000000",
--               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
--               usr_func_rd,usr_cs_rd,usr_rden);



    -- reset the receiver core
    wait for 2us;
    PCIe_func(pcie_clk,SR_RecSERDES_reset,address_offset,'0',x"0000000000010003",
              usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
              usr_func_rd,usr_cs_rd,usr_rden);
    wait for 80 ns;
    PCIe_func(pcie_clk,SR_RecSERDES_reset,address_offset,'0',x"0000000000010000",
              usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
              usr_func_rd,usr_cs_rd,usr_rden);  
 
    
    wait for 45 us;
    PCIe_func( pcie_clk,Slinkrocket_access_command,address_offset,'0', x"0000000080010006",
               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
               usr_func_rd,usr_cs_rd,usr_rden);
    wait for 1 us;
    PCIe_func( pcie_clk,Slinkrocket_access_data,address_offset,'0', x"0000000070000001",
               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
               usr_func_rd,usr_cs_rd,usr_rden);

    wait for 100ns;
    PCIe_func( pcie_clk, adr_LED_test, 0, '1', x"0000000000000000",
               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
               usr_func_rd,usr_cs_rd,usr_rden);

    wait for 100ns;
    PCIe_func( pcie_clk, adr_LED_test, 0, '0', x"0000000000000003",
               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
               usr_func_rd,usr_cs_rd,usr_rden);

    wait for 100ns;
    PCIe_func( pcie_clk, adr_LED_test, 0, '1', x"0000000000000000",
               usr_func_wr,usr_cs_wr,usr_wen,usr_data_wr,
               usr_func_rd,usr_cs_rd,usr_rden);

    wait;

  end process;
  
  
end Behavioral;
