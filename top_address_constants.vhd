library IEEE;
library WORK;
--use IEEE.std_logic_1164.all;
--use IEEE.std_logic_arith.all;

--use ieee.numeric_std.all;

-- the constants should be multiply by 8 to obtain the address offsets
-- for the items (64 bit words)

package top_address_constants is

  -- bit 0: soft reset
  constant        adr_system           : integer := 16#0000#;
  
  -- trivial LED test: bits 0 and 1 are connected to LEDs 0 and 1
  -- of the development board. Set to '1' to light the LEDs.
  constant        adr_LED_test         : integer := 16#0001#;

  -- adr_reset_cnt address resets all counters to 0
  constant        adr_reset_cnt        : integer := 16#0002#;

  -- readout counters in the receiver. A generic in the receiver forms
  -- an address offset for various slink channels in case that more than
  -- one slink is implemented in the design. The offset should be nx16
  constant        adr_packet_counter   : integer := 16#0003#;  
  constant        adr_bad_counter      : integer := 16#0004#;  
  constant        adr_backpr_counter   : integer := 16#0005#;  
  constant        adr_fedcrc_counter   : integer := 16#0006#;  
  constant        adr_slinkcrc_counter : integer := 16#0007#;  

  constant        adr_sender_status    : integer := 16#0008#;
  constant        adr_sender_status_c0    : integer := 16#0008#;
  constant        adr_sender_status_c1    : integer := 16#0018#;
  constant        adr_sender_status_c2    : integer := 16#0028#;
  constant        adr_sender_status_c3    : integer := 16#0038#;
    
end package top_address_constants;
