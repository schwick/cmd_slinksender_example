library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all; 
use WORK.top_address_constants.all;

entity MonitoringCounters is
  generic(
    adr_offset : integer
    );
  port(
    fed_clk           : in std_logic;
    reset_counters    : in std_logic;
    ena_cnt_pckt      : in std_logic;
    ena_cnt_bad       : in std_logic;
    ena_cnt_backpr    : in std_logic;
    ena_cnt_fedcrc    : in std_logic;
    ena_cnt_slinkcrc  : in std_logic;
    usr_func_rd       : in std_logic_vector( 16383 downto 0 );
    counter_data      : out std_logic_vector( 63 downto 0 );
    readCounterActive : out std_logic
    );
end MonitoringCounters;

architecture Behavioral of MonitoringCounters is

  component Counter
    port(
      clk   : in std_logic;
      data  : out std_logic_vector( 63 downto 0 );
      cnten : in std_logic;
      reset : in std_logic
      );
  end component;

  -- counter values
  signal pckt_cnt          : std_logic_vector( 63 downto 0 );
  signal bad_cnt           : std_logic_vector( 63 downto 0 );
  signal backpr_cnt        : std_logic_vector( 63 downto 0 );
  signal fedcrc_cnt        : std_logic_vector( 63 downto 0 );
  signal slinkcrc_cnt      : std_logic_vector( 63 downto 0 );

  
begin
  
-- packet counter
  pcktCounter : Counter
    port map (
      clk   => fed_clk,
      data  => pckt_cnt,
      cnten => ena_cnt_pckt,
      reset => reset_counters
      );

  -- bad counter  
  badCounter : Counter
    port map (
      clk   => fed_clk,
      data  => bad_cnt,
      cnten => ena_cnt_bad,
      reset => reset_counters
      );

  -- backpressure counter  
  backprCounter : Counter
    port map (
      clk   => fed_clk,
      data  => backpr_cnt,
      cnten => ena_cnt_backpr,
      reset => reset_counters
      );

  -- fed crc error counter  
  fedcrcCounter : Counter
    port map (
      clk   => fed_clk,
      data  => fedcrc_cnt,
      cnten => ena_cnt_fedcrc,
      reset => reset_counters
      );

  -- slink crc error counter  
  slinkcrcCounter : Counter
    port map (
      clk   => fed_clk,
      data  => slinkcrc_cnt,
      cnten => ena_cnt_slinkcrc,
      reset => reset_counters
      );

  rd_mplx : process( usr_func_rd, pckt_cnt, bad_cnt, backpr_cnt, fedcrc_cnt, slinkcrc_cnt )
  begin
    counter_data <= (others => '0');
    readCounterActive <= '0';
    
    if usr_func_rd(adr_packet_counter + adr_offset ) = '1' then
      counter_data <= pckt_cnt;
      readCounterActive <= '1';
    elsif usr_func_rd(adr_bad_counter + adr_offset ) = '1' then
      counter_data <= bad_cnt;
      readCounterActive <= '1';
    elsif usr_func_rd(adr_backpr_counter + adr_offset ) = '1' then
      counter_data <= backpr_cnt;
      readCounterActive <= '1';
    elsif usr_func_rd(adr_fedcrc_counter + adr_offset ) = '1' then
      counter_data <= fedcrc_cnt;
      readCounterActive <= '1';
    elsif usr_func_rd(adr_slinkcrc_counter + adr_offset ) = '1' then
      counter_data <= slinkcrc_cnt;
      readCounterActive <= '1';
    end if;

  end process;

end Behavioral;
