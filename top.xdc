create_clock -period 10.000 -name pcie_clk1_p -waveform {0.000 5.000} [get_ports {pcie_clk1_p}]
create_clock -period 6.400 -name qpll_ref_clock_p -waveform {0.000 3.200} [get_ports {qpll_ref_clock_p}]


set_property PACKAGE_PIN BB24     [get_ports "GPIO_SW_N"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L5P_T0U_N8_AD14P_64
set_property IOSTANDARD  LVCMOS18 [get_ports "GPIO_SW_N"] ;# Bank  64 VCCO - VCC1V8_FPGA - IO_L5P_T0U_N8_AD14P_64
set_property PACKAGE_PIN D12      [get_ports "u250mhz_clk1_n"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L13N_T2L_N1_GC_QBC_71
set_property IOSTANDARD  DIFF_SSTL12 [get_ports "u250mhz_clk1_n"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L13N_T2L_N1_GC_QBC_71
set_property PACKAGE_PIN E12      [get_ports "u250mhz_clk1_p"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_71
set_property IOSTANDARD  DIFF_SSTL12 [get_ports "u250mhz_clk1_p"] ;# Bank  71 VCCO - VCC1V2_FPGA - IO_L13P_T2L_N0_GC_QBC_71
set_property PACKAGE_PIN W9       [get_ports "qpll_ref_clock_p"] ;# Bank 231 - MGTREFCLK0P_231
set_property PACKAGE_PIN W8       [get_ports "qpll_ref_clock_n"] ;# Bank 231 - MGTREFCLK0N_231
#set_property PACKAGE_PIN G5       [get_ports "firefly_tx1_p"];
#set_property PACKAGE_PIN G4       [get_ports "firefly_tx1_n"];
#set_property PACKAGE_PIN K2       [get_ports "firefly_rx1_p"];
#set_property PACKAGE_PIN K1       [get_ports "firefly_rx1_n"];

set_property PACKAGE_PIN V7       [get_ports "qsfp1_tx1_p"];
set_property PACKAGE_PIN V6       [get_ports "qsfp1_tx1_n"];
set_property PACKAGE_PIN Y2       [get_ports "qsfp1_rx1_p"];
set_property PACKAGE_PIN Y1       [get_ports "qsfp1_rx1_n"];

set_property PACKAGE_PIN L5       [get_ports "qsfp2_tx1_p"] ;# bank 232 - mgtytxp0_232
set_property PACKAGE_PIN T2       [get_ports "qsfp2_rx1_p"] ;# BANK 232 - MGTYRXP0_232
set_property PACKAGE_PIN T1       [get_ports "qsfp2_rx1_n"] ;# BANK 232 - MGTYRXN0_232
set_property PACKAGE_PIN L4       [get_ports "qsfp2_tx1_n"] ;# Bank 232 - MGTYTXN0_232

set_property PACKAGE_PIN AL9      [get_ports "pcie_clk1_p"] ;# Bank 225 - MGTREFCLK0P_225
set_property PACKAGE_PIN AL8      [get_ports "pcie_clk1_n"] ;# Bank 225 - MGTREFCLK0N_225
# ac8(n) ac9(p) for pcieclock 2. dominique used that

set_property PACKAGE_PIN AM17     [get_ports pcie_rst];
set_property IOSTANDARD LVCMOS18  [get_ports pcie_rst];

set_property PACKAGE_PIN Y7       [get_ports "pcie_tx0_p"] ;# Bank 227 - MGTYTXP3_227
set_property PACKAGE_PIN AA4      [get_ports "pcie_rx0_p"] ;# Bank 227 - MGTYRXP3_227
set_property PACKAGE_PIN AA3      [get_ports "pcie_rx0_n"] ;# Bank 227 - MGTYRXN3_227
set_property PACKAGE_PIN Y6       [get_ports "pcie_tx0_n"] ;# Bank 227 - MGTYTXN3_227

set_property PACKAGE_PIN AT32     [get_ports "GPIO_LED0"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_40
set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED0"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_40
set_property PACKAGE_PIN AV34     [get_ports "GPIO_LED1"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_T2U_N12_40
set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED1"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_T2U_N12_40
#set_property PACKAGE_PIN AY30     [get_ports "GPIO_LED2"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_T1U_N12_40
#set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED2"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_T1U_N12_40
#set_property PACKAGE_PIN BB32     [get_ports "GPIO_LED3"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L7N_T1L_N1_QBC_AD13N_40
#set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED3"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L7N_T1L_N1_QBC_AD13N_40
#set_property PACKAGE_PIN BF32     [get_ports "GPIO_LED4"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_40
#set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED4"] ;# Bank  40 VCCO - VCC1V2_FPGA - IO_L1N_T0L_N1_DBC_40
#set_property PACKAGE_PIN AU37     [get_ports "GPIO_LED5"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_T3U_N12_42
#set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED5"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_T3U_N12_42
set_property PACKAGE_PIN AV36     [get_ports "GPIO_LED6"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_42
set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED6"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_L19N_T3L_N1_DBC_AD9N_42
set_property PACKAGE_PIN BA37     [get_ports "GPIO_LED7"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_L13N_T2L_N1_GC_QBC_42
set_property IOSTANDARD  LVCMOS12 [get_ports "GPIO_LED7"] ;# Bank  42 VCCO - VCC1V2_FPGA - IO_L13N_T2L_N1_GC_QBC_42


# set_multi_cycle_path 4 -setup -from [ get_pins my_pcie_interface/decode_rd/reg_reg[*]/C] -to [get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/USR_dto_rg_reg[*]/R ]
# set_multi_cycle_path 3 -hold -end -from [ get_pins my_pcie_interface/decode_rd/reg_reg[*]/C] -to [get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/USR_dto_rg_reg[*]/R ]
# 
# set_multi_cycle_path 4 -setup -from [ get_pins slink_interface/read_data[*] ] -to [ get_pins usr_data_rd*[*]/D 
# set_multi_cycle_path 3 -hold -end -from [ get_pins slink_interface/read_data[*] ] -to [ get_pins usr_data_rd*[*]/D 

set_false_path -from [ get_pins slink_interface/status_adr_reg[*]/C ]                                          -to [ get_pins usr_data_rd_reg[*]/D ]
set_false_path -from [ get_pins slink_interface/slinkSender/U0/Sender_core_i1/i1/status_data_reg[*]/C ]        -to [ get_pins usr_data_rd_reg[*]/D ]
set_false_path -from [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/control_dto_rg_reg[*]/C ] -to [ get_pins usr_data_rd_reg[*]/D ]
set_false_path -from [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/USR_dto_rg_reg[*]/C ]    -to [ get_pins usr_data_rd_reg[*]/D ]

set_false_path -from [ get_pins my_pcie_interface/decode_rd/reg_reg[*]/C]                                      -to [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/USR_dto_rg_reg[*]/R ]
set_false_path -from [ get_pins my_pcie_interface/decode_WR/reg_reg[*]/C ]                                     -to [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/Cmd_OL_reg[*]/CE ]
set_false_path -from [ get_pins my_pcie_interface/decode_WR/reg_reg[*]/C ]                                     -to [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/data_ol_reg[*]/CE ]
set_false_path -from [ get_pins my_pcie_interface/data_wr_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txpostcursor_in_reg[*]/D ]
set_false_path -from [ get_pins my_pcie_interface/data_wr_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txprecursor_in_reg[*]/D ]
set_false_path -from [ get_pins my_pcie_interface/data_wr_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txdiffctrl_in_reg[*]/D ]
set_false_path -from [ get_pins my_pcie_interface/data_wr_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/data_ol_reg[*]/D ]

set_false_path -from [ get_pins my_pcie_interface/usr_wen_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/drp_addr_reg[*]/CE ]
set_false_path -from [ get_pins my_pcie_interface/usr_wen_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/drp_di_reg[*]/CE ]

set_false_path -from [ get_pins my_pcie_interface/data_wr_reg_reg[*]/C ]                                       -to [ get_pins slink_interface/slink_receiver/U0/Receiver_core_i1/i4/rd_req_reg/D ]

set_false_path -from [ get_pins my_pcie_interface/decode_WR/reg_reg[*]/C ]                                     -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txpostcursor_in_reg[*]/CE ]
set_false_path -from [ get_pins my_pcie_interface/decode_WR/reg_reg[*]/C ]                                     -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txprecursor_in_reg[*]/CE ]
set_false_path -from [ get_pins my_pcie_interface/decode_WR/reg_reg[*]/C ]                                     -to [ get_pins slink_interface/slink_receiver/U0/serdes_receiver_i1/txdiffctrl_in_reg[*]/CE ]


# set_clock_groups -asynchonous -group [ get_clocks {usr_clk}] -group [ get_clocks { clk_out1_clk_wiz_0 } ]
#
#
# set_false_path -from [ get_pins slink_interface/slinkSender/U0/serdes_sender_i1/serdes_i1/inst/gen_gtwizard_gtye4_top.SlinkRocket_SERDES_Main_gtwizard_gtye4_inst/gen_gtwizard_gtye4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/*/C ]                                       -to [ get_pins usr_data_rd_reg[*]/D ]

