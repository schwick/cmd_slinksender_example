library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all; 

entity EventGenerator is
  generic (EVENT_SIZE : integer := 16#8#);
  Port (
    clk            : in std_logic;
    reset          : in std_logic;
    lff            : in std_logic;
    link_down_n    : in std_logic;
    event_data     : out std_logic_vector(127 downto 0);
    event_data_wen : out std_logic;
    event_ctrl     : out std_logic
    );

end EventGenerator;




architecture Behavioral of EventGenerator is

  component FED_fragment_CRC16_D128b
    port (
      clk         : in std_logic;
      clear_p     : in std_logic;
      Data        : in std_logic_vector( 127 downto 0 );
      enable      : in std_logic;
      CRC_out     : out std_logic_vector( 15 downto 0 )
      );
  end component;


  signal rcrc             : std_logic;
  -- holds the crc to be multiplexed on data bus when trailer is transferred
  signal crc_value        : std_logic_vector( 15 downto 0 );


  -- counters
  -- counters to generate payload: the low one counts always the high one only
  -- when data can be written. Hence it can be used to measure the backpressure.
  signal count_low      : std_logic_vector ( 63 downto 0 );
  signal count_high     : std_logic_vector ( 63 downto 0 );
  
  signal incr_high      : std_logic; --increment payload counter (MSW)
  
  -- event counter
  signal evtcnt         : std_logic_vector( 31 downto 0 );
  signal incr_evtcnt    :  std_logic;
  
  -- counts the size of the event (backwards to 0)
  -- evtSize holds a hardcoded event size
  signal sizecnt        : std_logic_vector( 31 downto 4 );
  signal evtSize        : std_logic_vector( 31 downto 4 );
  signal decr_sizecnt   : std_logic;


  -- SLINK interface
  signal event_data_0       : std_logic_vector( 127 downto 0 );
  signal event_data_1     : std_logic_vector( 127 downto 0);
  signal event_data_2     : std_logic_vector( 127 downto 0);
  signal event_ctrl_0     : std_logic;
  signal event_ctrl_1     : std_logic;
  signal event_trailer    : std_logic;
  signal event_data_wen_0 : std_logic;
  signal event_data_wen_1 : std_logic;
  signal event_trailer_1  : std_logic;

  -- event generator state-machine
  type evgen_state_type is (s_idle, s_header, s_payload, s_trailer, s_reset);
  signal evgen_state, next_evgen_state : evgen_state_type;
  signal canwrite       : std_logic;
  signal ena_payload    : std_logic;
  signal ena_header     : std_logic;
  signal ena_trailer    : std_logic;
  signal ena_crc        : std_logic;
  signal reset_crc      : std_logic;
  signal reset_crc_1    : std_logic;

  signal evt_size        : std_logic_vector( 31 downto 0 );
  
-------------------------- Sender part -----------------------------------
begin
  
  rcrc <= reset_crc_1 or reset;

  slinkcrc_i:FED_fragment_CRC16_D128b
    port map ( 
      clear_p  => rcrc,
      clk      => clk,
      enable   => ena_crc,
      Data     => event_data_0( 127 downto 0 ),
      CRC_out  => crc_value
      );
  
  
  -- some preliminary signal fixing
  process( clk )
  begin
    if rising_edge(clk) then
      canwrite <= (not lff and link_down_n);
    end if;
  end process;
  
  evtSize <= std_logic_vector( to_unsigned( EVENT_SIZE, evtSize'length));
  
  -- Counter for higher 64 bits of payload:  only count the clocks which are really written to the slink.
  process ( clk )
  begin
    if rising_edge(clk) then
      if ( reset = '1' ) then
        count_high <= (others => '0' );
      else
        if ( incr_high = '1' ) then
          count_high <= count_high + '1';
        end if;
      end if;
    end if;
  end process;
  
  -- Counter for lower 64 bits of payload: count all clocks    
  process ( clk )
  begin
    if rising_edge(clk) then
      if ( reset = '1' ) then
        count_low <= (others=>'0');
      else
        count_low <= count_low + '1';
      end if;
    end if;
  end process;
  
  -- Event counter
  process ( clk )
  begin
    if (rising_edge(clk)) then
      if ( reset = '1' ) then
        evtcnt <= std_logic_vector(to_unsigned(1, evtcnt'length));
      else
        if ( incr_evtcnt = '1' ) then
          evtcnt <= evtcnt + '1';
        end if;
      end if;
    end if;
  end process;
  
  -- Event Size counter : counts backwards
  
  process( clk )
  begin
    if (rising_edge(clk)) then
      if( reset = '1' ) then
        sizecnt <= evtSize;
      elsif( sizecnt = 0 ) then
        sizecnt <= evtSize;
      elsif( decr_sizecnt = '1' ) then
        sizecnt <= sizecnt - '1';
      end if;
      if ( sizecnt = 1 ) then
      end if;
    end if;
  end process;
  
  ------------------- State machine for the event generator -------------------------------
  evgen_sync : process (clk)
  begin
    if rising_edge(clk) then
      if ( reset = '1' ) then
        evgen_state <= s_idle;
      else
        evgen_state <= next_evgen_state;
      end if;
    end if;
  end process;
  
  next_state_decode : process( evgen_state, canwrite, sizecnt )
  begin
    next_evgen_state <= evgen_state;
    case (evgen_state) is 
      when s_idle =>
        if (canwrite = '1') then
          next_evgen_state <= s_header;
        end if;
      when ( s_header ) =>
        if ( canwrite = '1' ) then
          next_evgen_state <= s_payload;
        end if;
      when ( s_payload ) =>
        if ( canwrite = '1' and sizecnt = 1 ) then
          next_evgen_state <= s_trailer;
        end if;
      when ( s_trailer ) =>
        if ( canwrite = '1' ) then
          next_evgen_state <= s_reset;
        end if;
      when ( s_reset ) =>
        next_evgen_state <= s_idle;
    end case;
  end process;
  
  evtgen_output : process(evgen_state, canwrite)
  begin
    ena_header <= '0';
    ena_trailer <= '0';
    ena_crc <= '0';
    reset_crc <= '0';
    ena_payload <= '0';
    
    case( evgen_state ) is
      
      when( s_idle ) =>
        reset_crc <= '0';
        
      when( s_header ) =>
        if ( canwrite = '1' ) then
          ena_header <= '1';
          ena_crc <= '1';
        end if;
        
      when( s_trailer ) =>
        if ( canwrite = '1' ) then
          ena_trailer <= '1';
          ena_crc <= '1';
        end if;
        
      when( s_reset ) =>
        reset_crc <= '1';
        
      when( s_payload ) =>
        if ( canwrite = '1' ) then
          ena_payload <= '1';
          ena_crc <= '1';
        end if; 
    end case;
  end process;
  
  
  -- Multiplexer on the event_data_0 bus
  evtdata_mux : process( ena_header, ena_trailer, ena_payload, evtcnt, sizecnt, crc_value, count_high, count_low )
  begin
    event_data_0     <= ( others => '0' );
    event_ctrl_0     <= '0';
    event_trailer    <= '0';
    incr_evtcnt      <= '0';
    decr_sizecnt     <= '0';
    event_data_wen_0 <= '0';
    incr_high <= '0';
    if ( ena_header = '1' ) then
      event_data_0( 127 downto 120 ) <= x"55";
      event_data_0( 119 downto 096 ) <= x"000000";
      event_data_0( 095 downto 064 ) <= evtcnt;
      event_data_0( 063 downto 032 ) <= x"00000123";
      event_data_0( 031 downto 000 ) <= x"deadface";
      event_ctrl_0                   <= '1';
      event_data_wen_0               <= '1';
    elsif ( ena_trailer = '1' ) then
      event_data_0( 127 downto 120 ) <= x"AA";
      event_data_0( 119 downto 092 ) <= x"0000000";
      event_data_0( 091 downto 064 ) <= sizecnt;
      event_data_0( 063 downto 048 ) <= x"0000";
      event_data_0( 047 downto 000 ) <= x"000000000000";
      event_ctrl_0                   <= '1';
      event_trailer                  <= '1';
      incr_evtcnt                    <= '1';
      event_data_wen_0               <= '1';
    elsif ( ena_payload = '1' ) then
      event_data_0( 127 downto 64 ) <= count_high;
      event_data_0(  63 downto  0 ) <= count_low;
      event_data_wen_0              <= '1';
      incr_high                     <= '1';
      decr_sizecnt                  <= '1';
    end if;      
  end process;
  
  -- Register to compose the final data and to multiplex the crc in
  crcmultiplex : process( event_trailer, event_data_0( 063 downto 048 ), clk )
  begin
    if rising_edge( clk ) then
      
      event_ctrl_1     <= event_ctrl_0;
      event_trailer_1  <= event_trailer;
      event_data_wen_1 <= event_data_wen_0;
      event_ctrl       <= event_ctrl_1;
      event_data_wen   <= event_data_wen_1;
      event_data_1     <= event_data_0;
      
      reset_crc_1 <= reset_crc;
      event_data_2( 127 downto 64 )  <= event_data_1( 127 downto 64 );
      event_data_2( 47 downto 0 )    <= event_data_1( 47 downto 0 );
      if event_trailer_1 = '1' then
        event_data_2( 63 downto 48 ) <= crc_value;
      else
        event_data_2( 63 downto 48 ) <= event_data_0( 63 downto 48 );
      end if;
      
    end if;
  end process;
  
end Behavioral;

