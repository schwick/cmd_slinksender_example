#!/bin/bash

usage () {
    echo "Usage: $0 [ -v VERSION ]" 1>&2 
    exit 1
}

version=""

while getopts ":v:" opt; do
    case ${opt} in
	v )
	    version=${OPTARG}
	    ;;
	: )
	    echo "Error: -${OPTARG} requires an argument."
	    usage
	    ;;
	* )                                     # If unknown (any other) option:
	    usage
	    ;;
    esac
done

if [ -z ${version} ]
then
    usage
    exit 1
fi

echo "Version is ${version}"

rm -rf mrel
mkdir mrel
cd mrel
mkdir -p gitreps
#mkdir -p CMD_IPs/{SlinkSender_IP_virtex,SlinkReceiver_IP_virtex,SlinkSender_IP_kintex,SlinkReceiver_IP_kintex}
#cp ../../../../CMD_IPs/SlinkSender_IP_virtex/*.zip CMD_IPs/SlinkSender_IP_virtex/.
#cp ../../../../CMD_IPs/SlinkReceiver_IP_virtex/*.zip CMD_IPs/SlinkReceiver_IP_virtex/.
#cp ../../../../CMD_IPs/SlinkSender_IP_kintex/*.zip CMD_IPs/SlinkSender_IP_kintex/.
#cp ../../../../CMD_IPs/SlinkReceiver_IP_kintex/*.zip CMD_IPs/SlinkReceiver_IP_kintex/.
mkdir  tools
cp ../*.tcl tools/.
cp ../../documentation/SlinkRocketUG.pdf .

tar -czvf ../SlinkRocketExample_${version}.tgz ./
