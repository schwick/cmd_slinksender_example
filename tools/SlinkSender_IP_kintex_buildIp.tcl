create_project SlinkSender_IP_kintex ../../../build_IP/SlinkSender_IP_kintex -part xcvu9p-flga2104-2l-e
import_files {../../../gitreps/slinkrocket/Commun/FED_Fragment_CRC16_D128b.vhd ../../../gitreps/slinkrocket/Commun/Slink_packet_CRC16_D128b.vhd ../../../gitreps/slinkrocket/Commun/memory_dp.vhd ../../../gitreps/slinkrocket/SLINK_sender/Event_generator_lite.vhd ../../../gitreps/slinkrocket/SLINK_sender/FIFO_sync.vhd ../../../gitreps/slinkrocket/SLINK_sender/SLINKRocket_sender.vhd ../../../gitreps/slinkrocket/SLINK_sender/SR_sender_glb.vhd ../../../gitreps/slinkrocket/SLINK_sender/build_pckt_s.vhd ../../../gitreps/slinkrocket/SLINK_sender/core_logic.vhd ../../../gitreps/slinkrocket/SLINK_sender/fed_itf.vhd ../../../gitreps/slinkrocket/SLINK_sender/freq_measure.vhd ../../../gitreps/slinkrocket/SLINK_sender/rcv_pckt_s.vhd ../../../gitreps/slinkrocket/SLINK_sender/versioning.vhd ../../../gitreps/fpga_library/resync/resetn_resync.vhd ../../../gitreps/fpga_library/resync/resetp_resync.vhd ../../../gitreps/fpga_library/resync/resync_sig_gen.vhd ../../../gitreps/fpga_library/resync/resync_v4.vhd ../../../gitreps/slinkrocket/SERDES_inst/Serdes_wrapper_selection.vhd ../../../gitreps/slinkrocket/SERDES_inst/Serdes_wrapper_snd_inst.vhd ../../../gitreps/slinkrocket/SERDES_inst/R127b_T_serdes_127b_decoding.vhd ../../../gitreps/slinkrocket/SERDES_inst/SR_descrambler.vhd ../../../gitreps/slinkrocket/SERDES_inst/SR_scrambler.vhd ../../../gitreps/slinkrocket/SERDES_inst/T127b_R_serdes_127b_encoding.vhd ../../../gitreps/slinkrocket/SERDES_inst/reset_serdes.vhd }
import_files {../../../gitreps/slinkrocket/Commun/IP/SR_DP_memory_core_IP/SR_DP_memory_core_IP.xci ../../../gitreps/slinkrocket/SLINK_sender/IP/SR_FIFO_sender.xci ../../../gitreps/slinkrocket/SERDES_inst/IP/SlinkRocket_SERDES_16G366_GTY/SlinkRocket_SERDES_16G366_GTY.xci ../../../gitreps/slinkrocket/SERDES_inst/IP/SlinkRocket_SERDES_25G78125_GTY/SlinkRocket_SERDES_25G78125_GTY.xci }
update_compile_order -fileset sources_1
upgrade_ip [get_ips ] -log ip_upgrade.log
ipx::package_project -root_dir ../../../build_IP/SlinkSender_IP_kintex/SlinkSender_IP_kintex.srcs/sources_1 -vendor CERN_CMS_CMD -library user -taxonomy {/CERN_CMS_DAQ } 
add_files -fileset constrs_1 -norecurse {../../../gitreps/slinkrocket/SLINK_sender/SR_sender.xdc ../../../gitreps/slinkrocket/SLINK_sender/clock_ooc.xdc }
import_files -fileset constrs_1 -norecurse {../../../gitreps/slinkrocket/SLINK_sender/SR_sender.xdc ../../../gitreps/slinkrocket/SLINK_sender/clock_ooc.xdc }
set_property USED_IN {synthesis implementation out_of_context} [ get_files -regexp .*_ooc.xdc ]
set_property core_revision 2 [ipx::current_core]
set_property supported_families {kintexu Production kintexuplus Production } [ipx::current_core]
if { [ file exists ipguibuilder_SlinkSender_IP_kintex.tcl ] } {
    source ipguibuilder_SlinkSender_IP_kintex.tcl 
}
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property ip_repo_paths ../../../build_IP/SlinkSender_IP_kintex/SlinkSender_IP_kintex.srcs/sources_1 [current_project]
update_ip_catalog
ipx::check_integrity -quiet [ipx::current_core]
ipx::archive_core ../../../build_IP/SlinkSender_IP_kintex/cern.ch_user_SlinkSender_IP_kintex_GLB_2.00.zip [ipx::current_core]
