create_project CMD_Slink_Example ../exampleProject/CMD_Slink_Example -part xcvu9p-flga2104-2l-e

set_property BOARD_PART xilinx.com:vcu118:part0:2.0 [current_project]
import_files -norecurse ../gitreps/cmd_slinksender_example/Level_2.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/PCIeSimulation.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/top_sim.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/top.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/pcie_decoder.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/PCIe_interface.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/top_address_constants.vhd
import_files -norecurse ../gitreps/cmd_slinksender_example/utils.vhd
import_files -norecurse ../gitreps/slinkrocket/SERDES_inst/QPLL_wrapper_SR25_GTY_ref15625.vhd

import_files -norecurse ../gitreps/slinkrocket/SERDES_inst/SR_gtwizard_ultrascale_v1_7_gtye4_common.v
import_files -norecurse ../gitreps/slinkrocket/SERDES_inst/SR25_qpll_wrapper_gtye4_REF15625.v
import_files -norecurse ../gitreps/slinkrocket/Commun/FED_Fragment_CRC16_D128b.vhd
import_files -norecurse ../gitreps/fpga_library/resync/resetn_gen.vhd

import_files -norecurse ../gitreps/cmd_slinksender_example/ips/axi_pcie3_0.xci
import_files -norecurse ../gitreps/cmd_slinksender_example/ips/clk_wiz_0.xci

add_files -fileset constrs_1 -norecurse ../gitreps/slinkrocket/SLINK_receiver/SR_receiver.xdc
import_files -fileset constrs_1 ../gitreps/slinkrocket/SLINK_receiver/SR_receiver.xdc
set_property used_in_synthesis false [ get_files SR_receiver.xdc]
add_files -fileset constrs_1 -norecurse ../gitreps/slinkrocket/SLINK_sender/SR_sender.xdc
import_files -fileset constrs_1 ../gitreps/slinkrocket/SLINK_sender/SR_sender.xdc
set_property used_in_synthesis false [ get_files SR_sender.xdc]
add_files -fileset constrs_1 -norecurse ../gitreps/cmd_slinksender_example/timing.xdc
import_files -fileset constrs_1 ../gitreps/cmd_slinksender_example/timing.xdc
set_property used_in_synthesis false [ get_files timing.xdc]
add_files -fileset constrs_1 -norecurse ../gitreps/cmd_slinksender_example/iopins.xdc
import_files -fileset constrs_1 ../gitreps/cmd_slinksender_example/iopins.xdc
set_property used_in_synthesis false [ get_files iopins.xdc]

upgrade_ip [get_ips ] -log ip_upgrade.log
export_ip_user_files -of_objects [get_ips ] -no_script -sync -force -quiet

update_compile_order -fileset sources_1

# ----------------

set_property top top_sim [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]
set_property used_in_simulation false [get_files  ../exampleProject/CMD_Slink_Example/CMD_Slink_Example.srcs/sources_1/imports/cmd_slinksender_example/top.vhd]
set_property used_in_synthesis false [get_files  ../exampleProject/CMD_Slink_Example/CMD_Slink_Example.srcs/sources_1/imports/cmd_slinksender_example/top_sim.vhd]
set_property -name {xsim.simulate.runtime} -value {70us} -objects [get_filesets sim_1]


source addIPs.tcl

source buildProject.tcl

