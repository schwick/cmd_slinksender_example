# Include a new IP repository to the project
set_property  ip_repo_paths  {../gitreps/slinkrocket_ips/virtex} [current_project]
update_ip_catalog
update_ip_catalog -rebuild

# Add sender IP

create_ip -name SR_sender_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_sender_GLB_0
create_ip -name SR_sender_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_sender_GLB_1
create_ip -name SR_sender_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_sender_GLB_2
create_ip -name SR_sender_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_sender_GLB_3
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_0/SR_sender_GLB_0.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_1/SR_sender_GLB_1.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_2/SR_sender_GLB_2.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_3/SR_sender_GLB_3.xci]

update_compile_order -fileset sources_1
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_0/SR_sender_GLB_0.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_1/SR_sender_GLB_1.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_2/SR_sender_GLB_2.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_3/SR_sender_GLB_3.xci]

catch { config_ip_cache -export [get_ips -all SR_sender_GLB_*] }
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_0/SR_sender_GLB_0.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_1/SR_sender_GLB_1.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_2/SR_sender_GLB_2.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_3/SR_sender_GLB_3.xci] -no_script -sync -force -quiet

create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_0/SR_sender_GLB_0.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_1/SR_sender_GLB_1.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_2/SR_sender_GLB_2.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_3/SR_sender_GLB_3.xci]

export_simulation -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_sender_GLB_*/SR_sender_GLB_*.xci] -directory ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files/sim_scripts -ip_user_files_dir ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files -ipstatic_source_dir ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files/ipstatic -lib_map_path [list {modelsim=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/modelsim} {questa=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/questa} {ies=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/ies} {xcelium=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/xcelium} {vcs=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/vcs} {riviera=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


# Add receiver IP
create_ip -name SR_Receive_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_Receive_GLB_0
create_ip -name SR_Receive_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_Receive_GLB_1
create_ip -name SR_Receive_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_Receive_GLB_2
create_ip -name SR_Receive_GLB -vendor CERN_CMS_CMD -library user -version 1.0 -module_name SR_Receive_GLB_3

# Set the parameter for the address offset in the example design
set_property -dict [list CONFIG.Add_Offset_SLINKR {256}] [get_ips SR_Receive_GLB_0]
set_property -dict [list CONFIG.Add_Offset_SLINKR {272}] [get_ips SR_Receive_GLB_1]
set_property -dict [list CONFIG.Add_Offset_SLINKR {288}] [get_ips SR_Receive_GLB_2]
set_property -dict [list CONFIG.Add_Offset_SLINKR {304}] [get_ips SR_Receive_GLB_3]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_0/SR_Receive_GLB_0.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_1/SR_Receive_GLB_1.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_2/SR_Receive_GLB_2.xci]
generate_target {instantiation_template} [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_3/SR_Receive_GLB_3.xci]
update_compile_order -fileset sources_1
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_0/SR_Receive_GLB_0.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_1/SR_Receive_GLB_1.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_2/SR_Receive_GLB_2.xci]
generate_target all [get_files  ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_3/SR_Receive_GLB_3.xci]

catch { config_ip_cache -export [get_ips -all SR_Receive_GLB_*] }
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_0/SR_Receive_GLB_0.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_1/SR_Receive_GLB_1.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_2/SR_Receive_GLB_2.xci] -no_script -sync -force -quiet
export_ip_user_files -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_3/SR_Receive_GLB_3.xci] -no_script -sync -force -quiet

create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_0/SR_Receive_GLB_0.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_1/SR_Receive_GLB_1.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_2/SR_Receive_GLB_2.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_3/SR_Receive_GLB_3.xci]

export_simulation -of_objects [get_files ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.srcs/sources_1/ip/SR_Receive_GLB_*/SR_Receive_GLB_*.xci] -directory ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files/sim_scripts -ip_user_files_dir ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files -ipstatic_source_dir ../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.ip_user_files/ipstatic -lib_map_path [list {modelsim=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/modelsim} {questa=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/questa} {ies=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/ies} {xcelium=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/xcelium} {vcs=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/vcs} {riviera=../exampleProject/CMD_Slink_Example_4c/CMD_Slink_Example.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

