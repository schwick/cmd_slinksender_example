#!/usr/bin/python
import os
from shutil import copy, rmtree
import zipfile
import subprocess
import argparse
import sys
from glob import glob

parser = argparse.ArgumentParser( description="re-generate IPs and copy the results" )
parser.add_argument( '--sender', action='store_true' )
parser.add_argument( '--receiver', action='store_true' )

ips = []
args = parser.parse_args()
if args.sender:
    ips.append( ["SlinkSender_IP_virtex", "virtex"] )
    ips.append( ["SlinkSender_IP_kintex", "kintex"] )
if args.receiver:
    ips.append( ["SlinkReceiver_IP_virtex", "virtex"] )
    ips.append( ["SlinkReceiver_IP_kintex", "kintex"] )

pathToRoot = os.path.join( "..","..",".." )
srcdir = os.path.join(pathToRoot,"build_IP")
dstdir = os.path.join(pathToRoot,"gitreps/slinkrocket_ips")

procs = []
for ip in ips:
    cmd = ["vivado", "-mode", "batch", "-source", ip[0] + "_buildIp.tcl"]
    # if the project already exists we have to remove it first
    pdir = os.path.join( srcdir, ip[0] )
    if os.path.isdir( pdir ):
        print("Removing old project dir for IP generation: " + pdir )
        rmtree( pdir )
    procs.append( subprocess.Popen(cmd) )
print("\n\n\n====> now waiting\n\n\n")
for p in procs:
    p.wait()
print("\n\n\n")

print ("copying the results to " + dstdir)

for ip in ips:
    src = os.path.join( srcdir, ip[0], "cern.ch_user_" + ip[0] + "_GLB_*.zip")
    print src
    srcfile = glob( src )[0]
    filename = os.path.basename( srcfile )

    dst = os.path.join( dstdir, ip[1], ip[0] )
    dstfile = os.path.join( dst, filename )

    print (src)
    print (srcfile)
    print (filename)
    print (dst)
    print (dstfile )
    if os.path.isdir (dst ):
        rmtree( dst )
    os.makedirs( dst )

    print ( "copying " + srcfile + " to " + dst )
    
    copy( srcfile, dst )
    
    zip = zipfile.ZipFile( dstfile, 'r' )
    zip.extractall( dst )
    zip.close()
