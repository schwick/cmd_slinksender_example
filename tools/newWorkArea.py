#!/usr/bin/python

from subprocess import check_output, CalledProcessError, STDOUT
import shutil
import os
from glob import glob
from shutil import rmtree, copy

tag = "v00.01"

rootdir = os.path.expanduser("~/Designs/SlinkIP")
print(rootdir)
if os.path.isdir( rootdir ):
    rmtree( rootdir )
os.mkdir( rootdir )

os.chdir(rootdir)

os.mkdir( "build_dir" )
os.mkdir( "build_IP" )
os.mkdir( "CMD_IPs" )
os.mkdir( "exampleProject" )
os.mkdir( "gitreps" )
scriptdir = os.path.join( rootdir, "scripts" )
os.mkdir( scriptdir )

os.chdir( "gitreps" )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/schwick/cmd_slinksender_example.git" ] )
print(out)
os.chdir( "cmd_slinksender_example" )
out = check_output( ["git", "checkout", tag ], stderr=STDOUT )
print(out)
os.chdir( ".." )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/dgigi/slinkrocket_preliminary.git" ] )
print(out)
os.chdir( "slinkrocket_preliminary" )
out = check_output( ["git", "checkout", tag ], stderr=STDOUT )
print(out)
os.chdir( ".." )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/dgigi/vhdl_custom_library.git" ] )
print(out)
os.chdir( "vhdl_custom_library" )
out = check_output( ["git", "checkout", tag ], stderr=STDOUT )
print(out)
os.chdir( ".." )

# make the IPs and copy them into the CMD_IP directories
print("Now making the IPs for SLINK sender and SLINK receiver")
os.chdir( "cmd_slinksender_example/tools" )
out = check_output( ["./cpIPs.py", "--sender", "--receiver"], stderr=STDOUT )
print out

# create the project
for f in glob("*.tcl"):
    shutil.copy( f, scriptdir )

os.chdir( scriptdir )
print( "Now creating and building the example project" )
out = check_output( [ "vivado", "-source", "CMD_Slink_Example_create_project.tcl" ], stderr=STDOUT )
print out
