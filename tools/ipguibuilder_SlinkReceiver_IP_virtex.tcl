set_property value 156.25 [ipx::get_user_parameters ref_clock -of_objects [ipx::current_core]]
set_property value 156.25 [ipx::get_hdl_parameters ref_clock -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_user_parameters ref_clock -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_hdl_parameters ref_clock -of_objects [ipx::current_core]]
set_property value_validation_type list [ipx::get_user_parameters ref_clock -of_objects [ipx::current_core]]
set_property value_validation_list {156.25 123.5} [ipx::get_user_parameters ref_clock -of_objects [ipx::current_core]]
ipgui::add_param -name {ref_clock} -component [ipx::current_core] -display_name {Ref Clock} -show_label {true} -show_range {true} -widget {comboBox}

set_property value 25.78125 [ipx::get_user_parameters throughput -of_objects [ipx::current_core]]
set_property value 25.78125 [ipx::get_hdl_parameters throughput -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_user_parameters throughput -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_hdl_parameters throughput -of_objects [ipx::current_core]]
set_property value_validation_type list [ipx::get_user_parameters throughput -of_objects [ipx::current_core]]
set_property value_validation_list {16.366 25.78125} [ipx::get_user_parameters throughput -of_objects [ipx::current_core]]
ipgui::add_param -name {throughput} -component [ipx::current_core] -display_name {Link Speed} -show_label {true} -show_range {true} -widget {comboBox}

set_property value GTY [ipx::get_user_parameters technology -of_objects [ipx::current_core]]
set_property value GTY [ipx::get_hdl_parameters technology -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_user_parameters technology -of_objects [ipx::current_core]]
set_property value_format string [ipx::get_hdl_parameters technology -of_objects [ipx::current_core]]
set_property value_validation_type list [ipx::get_user_parameters technology -of_objects [ipx::current_core]]
set_property value_validation_list {GTY} [ipx::get_user_parameters technology -of_objects [ipx::current_core]]
ipgui::add_param -name {technology} -component [ipx::current_core] -display_name {Xilinx Transceiver technonolgy} -show_label {true} -show_range {true} -widget {comboBox}

