# Synthesis

set_property constrset constrs_1 [get_runs synth_1]
set_property constrset constrs_1 [get_runs impl_1]

reset_run synth_1
launch_runs synth_1 -jobs 32

# Implementation
launch_runs impl_1 -to_step write_bitstream -jobs 32
