#!/usr/bin/python

from subprocess import check_output, CalledProcessError, STDOUT
import os
from shutil import rmtree, copy
import argparse
import sys

fpgatyp = "virtex"

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--version', required=True,
                    help='The version string of format vxx.yy')
args = parser.parse_args()

print args
version = args.version
print "version: ", version

release = "SlinkRocketExample_" + version + ".tgz"
trydir = os.path.expanduser("~/tryRelease")
print trydir

if os.path.isdir( trydir ):
    rmtree( trydir )
os.mkdir( trydir )
copy( release, trydir )

os.chdir(trydir)

out = check_output( ["tar", "-xzvf", release] )
print out

# os.chdir( os.path.join( trydir, "CMD_IPs", "SlinkSender_IP_" + fpgatyp ) )
# out = check_output( ["unzip", "*.zip"] )
# print out
# os.chdir( os.path.join( trydir, "CMD_IPs", "SlinkReceiver_IP_" + fpgatyp ) )
# out = check_output( ["unzip", "*.zip"] )
# print out

os.chdir( os.path.join( trydir, "gitreps" ) )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/schwick/cmd_slinksender_example.git" ] )
print out
os.chdir( "cmd_slinksender_example" )
out = check_output( ["git", "checkout", version ], stderr=STDOUT )
print out
os.chdir( ".." )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket.git" ] )
print out
os.chdir( "slinkrocket" )
out = check_output( ["git", "checkout", version ], stderr=STDOUT )
print out
os.chdir( ".." )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/dth_p1-v2/fpga_library.git" ] )
print out
os.chdir( "fpga_library" )
out = check_output( ["git", "checkout", version ], stderr=STDOUT )
print out
os.chdir( ".." )

out = check_output( ["git", "clone", "ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket_ips.git" ] )
print out
os.chdir( "slinkrocket_ips" )
out = check_output( ["git", "checkout", version ], stderr=STDOUT )
print out
os.chdir( ".." )

os.chdir( os.path.join( trydir, "tools" ) )
out = check_output( [ "vivado", "-source", "CMD_Slink_Example_create_project.tcl" ], stderr=STDOUT )
print out

